package Client;
import javax.swing.*;

import java.util.ArrayList;
import java.util.List;

public class Language extends ILanguage{

    private List<Observer> observers = new ArrayList<Observer>();
    private String language;

    public Language(String language)
    {
        this.language=language;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
        notifyAllObservers();
    }


    @Override
    public void attach(Observer observer){
        System.out.println("Observer adaugat "+observer.getLanguage());
        observers.add(observer);
    }
    @Override
    public void RemoveObserver(Observer observer)
    {
        observers.remove(observer);
    }

    @Override
    public void notifyAllObservers(){

        System.out.println("Limba "+this.language+" este setata!");
        for (Observer observer : observers) {
            observer.update(language);
        }
    }
}

