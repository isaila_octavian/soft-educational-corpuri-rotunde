package Client;

import javax.swing.*;
import java.awt.*;

public class FabricaCorpuri extends JPanel {
    private String nume;
    private Color culoare;
    private SimetrieSfera sferaSimetrie;
    private SimetrieCon connn;
    private SimetrieTrunchiCon trr;
    private SimetrieCilindru cil;

    public FabricaCorpuri()
    {}

    public FabricaCorpuri(String nume,SimetrieCon c,Color culoare)
    {
        this.nume=nume;
        this.connn=c;
        this.culoare=culoare;
    }

    public FabricaCorpuri(String nume,SimetrieTrunchiCon c,Color culoare)
    {
        this.nume=nume;
        this.trr=c;
        this.culoare=culoare;
    }

    public FabricaCorpuri(String nume,SimetrieCilindru c,Color culoare)
    {
        this.nume=nume;
        this.cil=c;
        this.culoare=culoare;
    }

    public FabricaCorpuri(String nume,SimetrieSfera simetrieSfera,Color culoare) {
        this.nume = nume;
        this.sferaSimetrie = simetrieSfera;
        this.culoare = culoare;
    }

    public FabricaCorpuri(String nume, Color culoare)
    {
        this.nume=nume;
        this.culoare=culoare;
    }

    public String getNume() {
        return nume;
    }

    public Color getCuloare() {
        return culoare;
    }

    public SimetrieSfera getSferaSimetrie() {
        return sferaSimetrie;
    }

    public void setCuloare(Color culoare) {
        this.culoare = culoare;
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        this.setBackground(Color.WHITE);

        if(this.nume.equals("SimetrieSfera")) {
            if (this.sferaSimetrie.getCoordonata2() > this.sferaSimetrie.getCoordonata6()) {
                //System.out.println("aici");
                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                /////////////////////////////////////////////////////////
                g.drawRect(this.sferaSimetrie.getCoordonata5(), this.sferaSimetrie.getCoordonata6(), this.sferaSimetrie.getLungime(), this.sferaSimetrie.getLatime());
                /////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() - 4 * (this.sferaSimetrie.getCoordonata2() - this.sferaSimetrie.getCoordonata6()), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2() - 4 * (this.sferaSimetrie.getCoordonata2() - this.sferaSimetrie.getCoordonata6()), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4 - 4 * (this.sferaSimetrie.getCoordonata2() - this.sferaSimetrie.getCoordonata6()), 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
            }
            else if (this.sferaSimetrie.getCoordonata2() < this.sferaSimetrie.getCoordonata6()) {
                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                /////////////////////////////////////////////////////////
                g.drawRect(this.sferaSimetrie.getCoordonata5(), this.sferaSimetrie.getCoordonata6(), this.sferaSimetrie.getLungime(), this.sferaSimetrie.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + (this.sferaSimetrie.getCoordonata6() - this.sferaSimetrie.getCoordonata2()), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2() + (this.sferaSimetrie.getCoordonata6() - this.sferaSimetrie.getCoordonata2()), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4 + (this.sferaSimetrie.getCoordonata6() - this.sferaSimetrie.getCoordonata2()), 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
            }
            else if (this.sferaSimetrie.getCoordonata1() > this.sferaSimetrie.getCoordonata5()) {
                int numar = this.sferaSimetrie.getCoordonata1() - this.sferaSimetrie.getCoordonata5() - this.sferaSimetrie.getLungime();
                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                /////////////////////////////////////////////////////////
                g.drawRect(this.sferaSimetrie.getCoordonata5(), this.sferaSimetrie.getCoordonata6(), this.sferaSimetrie.getLungime(), this.sferaSimetrie.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() - numar - 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() - numar - 2 * this.sferaSimetrie.getRaza() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() - numar - 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
            }
            else if (this.sferaSimetrie.getCoordonata1() < this.sferaSimetrie.getCoordonata5()) {
                int numar = this.sferaSimetrie.getCoordonata5() - this.sferaSimetrie.getCoordonata1() - 2 * this.sferaSimetrie.getRaza();
                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                /////////////////////////////////////////////////////////
                g.drawRect(this.sferaSimetrie.getCoordonata5(), this.sferaSimetrie.getCoordonata6(), this.sferaSimetrie.getLungime(), this.sferaSimetrie.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() + numar + this.sferaSimetrie.getLungime(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() + numar + 3 * this.sferaSimetrie.getRaza() / 4 + this.sferaSimetrie.getLungime(), this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() + numar + this.sferaSimetrie.getLungime(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
            }
        }
        else if(this.nume.equals("SimetrieCon"))
        {
            if(this.connn.getCoordonata2()>this.connn.getCoordonata6())
            {
                int numar=this.connn.getCoordonata2()-this.connn.getCoordonata6()-this.connn.getLatime();//de la baza pana la latura dr

                g.setColor(this.culoare);
                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata2()+this.connn.getRaza()/2);

                /////////////////////////////////////////////////////////
                g.drawRect(this.connn.getCoordonata5(), this.connn.getCoordonata6(), this.connn.getLungime(), this.connn.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata6()-numar-this.connn.getRaza(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata6()-numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()-numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata6()-numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()-numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata6()-numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()-numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata6()-numar-this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata6()-numar-this.connn.getRaza()/2);
            }
            else if(this.connn.getCoordonata2()<this.connn.getCoordonata6())
            {
                int numar=this.connn.getCoordonata6()-this.connn.getCoordonata2()+this.connn.getRaza()/2;//de la baza pana la latura dr

                g.setColor(this.culoare);
                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata2()+this.connn.getRaza()/2);

                /////////////////////////////////////////////////////////
                g.drawRect(this.connn.getCoordonata5(), this.connn.getCoordonata6(), this.connn.getLungime(), this.connn.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata6()+numar-this.connn.getRaza(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata6()+numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()+numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata6()+numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()+numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata6()+numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()+numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata6()+numar-this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata6()+numar-this.connn.getRaza()/2);
            }
            else if(this.connn.getCoordonata1()>this.connn.getCoordonata5())
            {
                int numar=this.connn.getCoordonata1()-this.connn.getCoordonata5()-this.connn.getLungime();
                int numar1=this.connn.getCoordonata3()-this.connn.getCoordonata5()-this.connn.getLungime();

                g.setColor(this.culoare);
                g.drawRoundRect(this.connn.getCoordonata5()-2*this.connn.getRaza()-numar, this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata5()-2*this.connn.getRaza()-numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()-numar1, this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata5()+2*this.connn.getRaza()-2*this.connn.getRaza()-numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()-numar1, this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata5()+this.connn.getRaza()-2*this.connn.getRaza()-numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()-numar1, this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata5()-2*this.connn.getRaza()-numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+2*this.connn.getRaza()-2*this.connn.getRaza()-numar, this.connn.getCoordonata2()+this.connn.getRaza()/2);

                /////////////////////////////////////////////////////////
                g.drawRect(this.connn.getCoordonata5(), this.connn.getCoordonata6(), this.connn.getLungime(), this.connn.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata2()+this.connn.getRaza()/2);
            }
            else if(this.connn.getCoordonata1()<this.connn.getCoordonata5())
            {
                int numar=this.connn.getCoordonata5()-this.connn.getCoordonata1()-2*this.connn.getRaza();
                int numar1=this.connn.getCoordonata5()-this.connn.getCoordonata3()+this.connn.getLungime();

                g.setColor(this.culoare);
                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata2()+this.connn.getRaza()/2);

                /////////////////////////////////////////////////////////
                g.drawRect(this.connn.getCoordonata5(), this.connn.getCoordonata6(), this.connn.getLungime(), this.connn.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.connn.getCoordonata5()+this.connn.getLungime()+numar, this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata5()+this.connn.getLungime()+numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+numar1, this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata5()+2*this.connn.getRaza()+this.connn.getLungime()+numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+numar1, this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata5()+this.connn.getLungime()+numar+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+numar1, this.connn.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.connn.getCoordonata5()+this.connn.getLungime()+numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+2*this.connn.getRaza()+this.connn.getLungime()+numar, this.connn.getCoordonata2()+this.connn.getRaza()/2);
            }
        }

        else if(this.nume.equals("SimetrieTrunchiCon")) {
            if (this.trr.getCoordonata2() > this.trr.getCoordonata6()) {
                int numar=this.trr.getCoordonata2()-this.trr.getCoordonata6();

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                /////////////////////////////////////////////////////////
                g.drawRect(this.trr.getCoordonata5(), this.trr.getCoordonata6(), this.trr.getLungime(), this.trr.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata6()-numar, this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata6()-numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata6()-numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata6()-numar  + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata6()-numar  + this.trr.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4())+ this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata6()-numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
            }
            else if (this.trr.getCoordonata2() < this.trr.getCoordonata6()) {
                int numar=this.trr.getCoordonata6()-this.trr.getCoordonata2();

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                /////////////////////////////////////////////////////////
                g.drawRect(this.trr.getCoordonata5(), this.trr.getCoordonata6(), this.trr.getLungime(), this.trr.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata6()+numar, this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata6()+numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata6()+numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata6()+numar  + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata6()+numar  + this.trr.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4())+ this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata6()+numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
            }
            else if (this.trr.getCoordonata1() > this.trr.getCoordonata5()) {
                int numar=this.trr.getCoordonata1()-this.trr.getCoordonata5()-this.trr.getLungime();
                int numar1=this.trr.getCoordonata3()-this.trr.getCoordonata5()-this.trr.getLungime();

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                /////////////////////////////////////////////////////////
                g.drawRect(this.trr.getCoordonata5(), this.trr.getCoordonata6(), this.trr.getLungime(), this.trr.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata5()-this.trr.getRaza1()-numar, this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata5()-this.trr.getRaza2()-numar1, this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza1()-numar, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()-this.trr.getRaza2()-numar1, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza1()-numar + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()-this.trr.getRaza2()-numar1 + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza1()-numar, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()-this.trr.getRaza1()-numar+ this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza2()-numar1 , this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata5()-this.trr.getRaza2()-numar1  + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza1()-numar+ this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()-this.trr.getRaza2()-numar1+ this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
            }
            else if(this.trr.getCoordonata1()<this.trr.getCoordonata5())
            {
                int numar=this.trr.getCoordonata5()-this.trr.getCoordonata1()-this.trr.getRaza1();
                int numar1=this.trr.getCoordonata5()-this.trr.getCoordonata3();

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                /////////////////////////////////////////////////////////
                g.drawRect(this.trr.getCoordonata5(), this.trr.getCoordonata6(), this.trr.getLungime(), this.trr.getLatime());
                ////////////////////////////////////////////////////////
                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata5()+this.trr.getLungime()+numar, this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.trr.getCoordonata5()+numar1, this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()+this.trr.getLungime()+numar, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()+numar1, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()+this.trr.getLungime()+numar+ this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()+numar1+ this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()+this.trr.getLungime()+numar, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()+this.trr.getLungime()+numar + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()+numar1, this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata5()+numar1 + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.trr.getCoordonata5()+this.trr.getLungime()+numar + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()+numar1 + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
            }
        }

        else if(this.nume.equals("SimetrieCilindru")) {
            if (this.cil.getCoordonata2() > this.cil.getCoordonata6()) {
                int numar=this.cil.getCoordonata2() -this.cil.getCoordonata6()-this.cil.getLatime();

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                /////////////////////////////////////////////////////////
                g.drawRect(this.cil.getCoordonata5(), this.cil.getCoordonata6(), this.cil.getLungime(), this.cil.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata6()-numar, this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata6() -this.cil.getH()-numar, this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6()-numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata6()-this.cil.getH()-numar + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()-numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()-this.cil.getH()-numar + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6()-numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()-numar + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6() -this.cil.getH()-numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6() -this.cil.getH()-numar + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata6()-numar+this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata6()-numar-this.cil.getH() + this.cil.getRaza1() / 4);
            }
            else if (this.cil.getCoordonata2() < this.cil.getCoordonata6()) {
                int numar=this.cil.getCoordonata6() -this.cil.getCoordonata2();

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                /////////////////////////////////////////////////////////
                g.drawRect(this.cil.getCoordonata5(), this.cil.getCoordonata6(), this.cil.getLungime(), this.cil.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata6()+numar, this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata6() +this.cil.getH()+numar, this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6()+numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata6()+this.cil.getH()+numar + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()+numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()+this.cil.getH()+numar + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6()+numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()+numar + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6() +this.cil.getH()+numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6() +this.cil.getH()+numar + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata6()+numar+this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata6()+numar+this.cil.getH() + this.cil.getRaza1() / 4);
            }
            else if (this.cil.getCoordonata1() > this.cil.getCoordonata5()) {
                int numar=this.cil.getCoordonata1() -this.cil.getCoordonata5()-this.cil.getLungime();

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                /////////////////////////////////////////////////////////
                g.drawRect(this.cil.getCoordonata5(), this.cil.getCoordonata6(), this.cil.getLungime(), this.cil.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
            }
            else if (this.cil.getCoordonata1() < this.cil.getCoordonata5()) {
                int numar=this.cil.getCoordonata5() -this.cil.getCoordonata1()-this.cil.getRaza1()+this.cil.getLungime();

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                /////////////////////////////////////////////////////////
                g.drawRect(this.cil.getCoordonata5(), this.cil.getCoordonata6(), this.cil.getLungime(), this.cil.getLatime());
                ////////////////////////////////////////////////////////

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawRoundRect(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()+numar + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);

                g.setColor(this.culoare);
                g.drawLine(this.cil.getCoordonata5()+numar + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
            }
        }
    }
}

