package Client;

public abstract class  ILanguage {
    abstract void attach(Observer observer);
    abstract void RemoveObserver(Observer observer);
    abstract public void notifyAllObservers();

}

