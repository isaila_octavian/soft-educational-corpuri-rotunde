package Client;

import javax.swing.*;
import java.awt.*;
import java.sql.*;

public class Conexiune{
    private Connection con;
    private Statement st;

    private Conexiune() {
        this.con = null;
        this.st = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/conectare";
            con = DriverManager.getConnection(url, "root", "");
            st = con.createStatement();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public Statement getSt() {
        return st;
    }

    public void setSt(Statement st) {
        this.st = st;
    }

    public void theQuery(String q) throws SQLException {
        st.executeUpdate(q);
    }

}
