package Client;

import javax.swing.*;
import java.awt.*;

public class FabricaDesenare extends JPanel {
    private String nume;
    private Color culoare;
    private int coordonata1;
    private int coordonata2;
    private int coordonata3;
    private double alfa;
    private double beta;
    private double gama;
    private int raza1;
    private int h;
    private int coordonata4;
    private int coordonata5;
    private int coordonata6;
    private int raza2;
    private int raza;

    public FabricaDesenare() {
    }

    public FabricaDesenare(String nume, Color culoare, int coordonata1, int coordonata2, int h, int raza1) {
        this.nume = nume;
        this.culoare = culoare;
        this.coordonata1 = coordonata1;
        this.coordonata2 = coordonata2;
        this.h = h;
        this.raza1 = raza1;
    }

    public FabricaDesenare(String nume, Color culoare, int coordonata1, int coordonata2, int coordonata3, int h, int raza1, double alfa, double beta, double gama) {
        this.nume = nume;
        this.culoare = culoare;
        this.coordonata1 = coordonata1;
        this.coordonata2 = coordonata2;
        this.coordonata3 = coordonata3;
        this.h = h;
        this.raza1 = raza1;
        this.alfa = alfa;
        this.beta = beta;
        this.gama = gama;

    }

    public FabricaDesenare(String nume, Color culoare, int coordonata1, int coordonata2, int coordonata3, int coordonata4, int raza1, int raza2) {
        this.nume = nume;
        this.culoare = culoare;
        this.coordonata1 = coordonata1;
        this.coordonata2 = coordonata2;
        this.coordonata3 = coordonata3;
        this.coordonata4 = coordonata4;
        this.raza1 = raza1;
        this.raza2 = raza2;
    }

    public FabricaDesenare(String nume, Color culoare, int coordonata1, int coordonata2, int coordonata3, int coordonata4, int coordonata5, int coordonata6, int raza1, int raza2, double alfa, double beta, double gama) {
        this.nume = nume;
        this.culoare = culoare;
        this.coordonata1 = coordonata1;
        this.coordonata2 = coordonata2;
        this.coordonata3 = coordonata3;
        this.coordonata4 = coordonata4;
        this.coordonata5 = coordonata5;
        this.coordonata6 = coordonata6;
        this.raza1 = raza1;
        this.raza2 = raza2;
        this.alfa = alfa;
        this.beta = beta;
        this.gama = gama;
    }

    public FabricaDesenare(String nume, Color culoare, int c1, int c2, int c3, int c4, int raza)
    {
        this.nume = nume;
        this.culoare = culoare;
        this.coordonata1=c1;
        this.coordonata2=c2;
        this.coordonata3=c3;
        this.coordonata4=c4;
        this.raza=raza;
    }

    public FabricaDesenare(String nume, Color culoare, int c1, int c2, int c3, int c4,int c5,int c6, int raza,double alfa,double beta,double gama)
    {
        this.nume = nume;
        this.culoare = culoare;
        this.coordonata1=c1;
        this.coordonata2=c2;
        this.coordonata3=c3;
        this.coordonata4=c4;
        this.coordonata5=c5;
        this.coordonata6=c6;
        this.raza=raza;
        this.alfa=alfa;
        this.beta=beta;
        this.gama=gama;
    }

    public FabricaDesenare(String nume, Color culoare, int coordonata1, int coordonata2, int raza)
    {
        this.nume = nume;
        this.culoare = culoare;
        this.coordonata1=coordonata1;
        this.coordonata2=coordonata2;
        this.raza=raza;
    }

    public FabricaDesenare(String nume, Color culoare, int coordonata1, int coordonata2,int coordonata3, int raza,double alfa,double beta,double gama)
    {
        this.nume = nume;
        this.culoare = culoare;
        this.coordonata1=coordonata1;
        this.coordonata2=coordonata2;
        this.coordonata3=coordonata3;
        this.raza=raza;
        this.alfa=alfa;
        this.beta=beta;
        this.gama=gama;
    }


    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Color getCuloare() {
        return culoare;
    }

    public void setCuloare(Color culoare) {
        this.culoare = culoare;
    }

    public int getCoordonata1() {
        return coordonata1;
    }

    public void setCoordonata1(int coordonata1) {
        this.coordonata1 = coordonata1;
    }

    public int getCoordonata2() {
        return coordonata2;
    }

    public void setCoordonata2(int coordonata2) {
        this.coordonata2 = coordonata2;
    }

    public int getCoordonata3() {
        return coordonata3;
    }

    public void setCoordonata3(int coordonata3) {
        this.coordonata3 = coordonata3;
    }

    public double getAlfa() {
        return alfa;
    }

    public void setAlfa(double alfa) {
        this.alfa = alfa;
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public double getGama() {
        return gama;
    }

    public void setGama(double gama) {
        this.gama = gama;
    }

    public int getRaza1() {
        return raza1;
    }

    public void setRaza1(int raza1) {
        this.raza1 = raza1;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getCoordonata4() {
        return coordonata4;
    }

    public void setCoordonata4(int coordonata4) {
        this.coordonata4 = coordonata4;
    }

    public int getCoordonata5() {
        return coordonata5;
    }

    public void setCoordonata5(int coordonata5) {
        this.coordonata5 = coordonata5;
    }

    public int getCoordonata6() {
        return coordonata6;
    }

    public void setCoordonata6(int coordonata6) {
        this.coordonata6 = coordonata6;
    }

    public int getRaza2() {
        return raza2;
    }

    public void setRaza2(int raza2) {
        this.raza2 = raza2;
    }

    public int getRaza() {
        return raza;
    }

    public void setRaza(int raza) {
        this.raza = raza;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.WHITE);

        if (this.nume.equals("Cilindru")) {
            g.setColor(this.culoare);
            g.drawRoundRect(this.getCoordonata1(), this.getCoordonata2(), this.getRaza1(), this.getRaza1() / 2, this.getRaza1(), this.getRaza1());

            g.setColor(this.culoare);
            g.drawRoundRect(this.getCoordonata1(), this.getCoordonata2() - this.getH(), this.getRaza1(), this.getRaza1() / 2, this.getRaza1(), this.getRaza1());

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1(), this.getCoordonata2() + this.getRaza1() / 4, this.getCoordonata1(), this.getCoordonata2() - this.getH() + this.getRaza1() / 4);

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1() + this.getRaza1(), this.getCoordonata2() + this.getRaza1() / 4, this.getCoordonata1() + this.getRaza1(), this.getCoordonata2() - this.getH() + this.getRaza1() / 4);

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1(), this.getCoordonata2() + this.getRaza1() / 4, this.getCoordonata1() + this.getRaza1(), this.getCoordonata2() + this.getRaza1() / 4);

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1(), this.getCoordonata2() - this.getH() + this.getRaza1() / 4, this.getCoordonata1() + this.getRaza1(), this.getCoordonata2() - this.getH() + this.getRaza1() / 4);

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1() + this.getRaza1() / 2, this.getCoordonata2() - this.getH() + this.getRaza1() / 4, this.getCoordonata1() + this.getRaza1() / 2, this.getCoordonata2() + this.getRaza1() / 4);
        } else if (this.nume.equals("TrunchiCon")) {
            g.setColor(this.culoare);
            g.drawRoundRect(this.getCoordonata1(), this.getCoordonata2(), this.getRaza1(), this.getRaza1() / 2, this.getCoordonata1(), this.getCoordonata1());

            g.setColor(this.culoare);
            g.drawRoundRect(this.getCoordonata3(), this.getCoordonata4(), this.getRaza2(), this.getRaza2() / 2, this.getCoordonata3(), this.getCoordonata3());

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1(), this.getCoordonata2() + this.getRaza1() / 4, this.getCoordonata3(), this.getCoordonata4() + this.getRaza2() / 4);

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1() + this.getRaza1(), this.getCoordonata2() + this.getRaza1() / 4, this.getCoordonata3() + this.getRaza2(), this.getCoordonata4() + this.getRaza2() / 4);

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1(), this.getCoordonata2() + this.getRaza1() / 4, this.getCoordonata1() + this.getRaza1(), this.getCoordonata2() + this.getRaza1() / 4);

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata3(), this.getCoordonata4() + this.getRaza2() / 4, this.getCoordonata3() + this.getRaza2(), this.getCoordonata4() + this.getRaza2() / 4);

            g.setColor(this.culoare);
            g.drawLine(this.getCoordonata1() + this.getRaza1() / 2, this.getCoordonata2() + this.getRaza1() / 4, this.getCoordonata3() + this.getRaza2() / 2, this.getCoordonata4() + this.getRaza2() / 4);
        }
        else if(this.nume.equals("Con"))
        {
            {
                g.setColor(this.culoare);
                g.drawRoundRect(this.getCoordonata1(), this.getCoordonata2(), 2*this.getRaza(), this.getRaza(), 2*this.getRaza(), 2*this.getRaza());

                g.setColor(this.culoare);
                g.drawLine(this.getCoordonata1(),this.getCoordonata2()+this.getRaza()/2, this.getCoordonata3(), this.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.getCoordonata1()+2*this.getRaza(),this.getCoordonata2()+this.getRaza()/2, this.getCoordonata3(), this.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.getCoordonata1()+this.getRaza(),this.getCoordonata2()+this.getRaza()/2, this.getCoordonata3(), this.getCoordonata4());

                g.setColor(this.culoare);
                g.drawLine(this.getCoordonata1(),this.getCoordonata2()+this.getRaza()/2, this.getCoordonata1()+2*this.getRaza(), this.getCoordonata2()+this.getRaza()/2);

            }
        }
        else if(this.nume.equals("Sfera"))
        {
            {
                g.setColor(this.culoare);
                g.drawRoundRect(this.getCoordonata1(),this.getCoordonata2(),2*this.getRaza(),2*this.getRaza(),2*this.getRaza(),2*this.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.getCoordonata1()+3*this.getRaza()/4,this.getCoordonata2(),this.getRaza()/2,2*this.getRaza(),2*this.getRaza(),2*this.getRaza());

                g.setColor(this.culoare);
                g.drawRoundRect(this.getCoordonata1(),this.getCoordonata2()+3*this.getRaza()/4,2*this.getRaza(),this.getRaza()/2,2*this.getRaza(),2*this.getRaza());
            }
        }
//        else if(this.nume.equals("SimetrieSfera")) {
//            if (this.sferaSimetrie.getCoordonata2() > this.sferaSimetrie.getCoordonata6()) {
//                //System.out.println("aici");
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.sferaSimetrie.getCoordonata5(), this.sferaSimetrie.getCoordonata6(), this.sferaSimetrie.getLungime(), this.sferaSimetrie.getLatime());
//                /////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() - 4 * (this.sferaSimetrie.getCoordonata2() - this.sferaSimetrie.getCoordonata6()), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2() - 4 * (this.sferaSimetrie.getCoordonata2() - this.sferaSimetrie.getCoordonata6()), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4 - 4 * (this.sferaSimetrie.getCoordonata2() - this.sferaSimetrie.getCoordonata6()), 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//            }
//            else if (this.sferaSimetrie.getCoordonata2() < this.sferaSimetrie.getCoordonata6()) {
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.sferaSimetrie.getCoordonata5(), this.sferaSimetrie.getCoordonata6(), this.sferaSimetrie.getLungime(), this.sferaSimetrie.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + (this.sferaSimetrie.getCoordonata6() - this.sferaSimetrie.getCoordonata2()), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2() + (this.sferaSimetrie.getCoordonata6() - this.sferaSimetrie.getCoordonata2()), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4 + (this.sferaSimetrie.getCoordonata6() - this.sferaSimetrie.getCoordonata2()), 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//            }
//            else if (this.sferaSimetrie.getCoordonata1() > this.sferaSimetrie.getCoordonata5()) {
//                int numar = this.sferaSimetrie.getCoordonata1() - this.sferaSimetrie.getCoordonata5() - this.sferaSimetrie.getLungime();
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.sferaSimetrie.getCoordonata5(), this.sferaSimetrie.getCoordonata6(), this.sferaSimetrie.getLungime(), this.sferaSimetrie.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() - numar - 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() - numar - 2 * this.sferaSimetrie.getRaza() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() - numar - 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//            }
//            else if (this.sferaSimetrie.getCoordonata1() < this.sferaSimetrie.getCoordonata5()) {
//                int numar = this.sferaSimetrie.getCoordonata5() - this.sferaSimetrie.getCoordonata1() - 2 * this.sferaSimetrie.getRaza();
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1() + 3 * this.sferaSimetrie.getRaza() / 4, this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata1(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.sferaSimetrie.getCoordonata5(), this.sferaSimetrie.getCoordonata6(), this.sferaSimetrie.getLungime(), this.sferaSimetrie.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() + numar + this.sferaSimetrie.getLungime(), this.sferaSimetrie.getCoordonata2(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() + numar + 3 * this.sferaSimetrie.getRaza() / 4 + this.sferaSimetrie.getLungime(), this.sferaSimetrie.getCoordonata2(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.sferaSimetrie.getCoordonata5() + numar + this.sferaSimetrie.getLungime(), this.sferaSimetrie.getCoordonata2() + 3 * this.sferaSimetrie.getRaza() / 4, 2 * this.sferaSimetrie.getRaza(), this.sferaSimetrie.getRaza() / 2, 2 * this.sferaSimetrie.getRaza(), 2 * this.sferaSimetrie.getRaza());
//            }
//        }
//        else if(this.nume.equals("SimetrieCon"))
//        {
//            if(this.connn.getCoordonata2()>this.connn.getCoordonata6())
//            {
//                int numar=this.connn.getCoordonata2()-this.connn.getCoordonata6()-this.connn.getLatime();//de la baza pana la latura dr
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata2()+this.connn.getRaza()/2);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.connn.getCoordonata5(), this.connn.getCoordonata6(), this.connn.getLungime(), this.connn.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata6()-numar-this.connn.getRaza(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata6()-numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()-numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata6()-numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()-numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata6()-numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()-numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata6()-numar-this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata6()-numar-this.connn.getRaza()/2);
//            }
//            else if(this.connn.getCoordonata2()<this.connn.getCoordonata6())
//            {
//                int numar=this.connn.getCoordonata6()-this.connn.getCoordonata2()+this.connn.getRaza()/2;//de la baza pana la latura dr
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata2()+this.connn.getRaza()/2);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.connn.getCoordonata5(), this.connn.getCoordonata6(), this.connn.getLungime(), this.connn.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata6()+numar-this.connn.getRaza(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata6()+numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()+numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata6()+numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()+numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata6()+numar-this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata6()+numar-(this.connn.getCoordonata2()-this.connn.getCoordonata4())-this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata6()+numar-this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata6()+numar-this.connn.getRaza()/2);
//            }
//            else if(this.connn.getCoordonata1()>this.connn.getCoordonata5())
//            {
//                int numar=this.connn.getCoordonata1()-this.connn.getCoordonata5()-this.connn.getLungime();
//                int numar1=this.connn.getCoordonata3()-this.connn.getCoordonata5()-this.connn.getLungime();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.connn.getCoordonata5()-2*this.connn.getRaza()-numar, this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata5()-2*this.connn.getRaza()-numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()-numar1, this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata5()+2*this.connn.getRaza()-2*this.connn.getRaza()-numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()-numar1, this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata5()+this.connn.getRaza()-2*this.connn.getRaza()-numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()-numar1, this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata5()-2*this.connn.getRaza()-numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+2*this.connn.getRaza()-2*this.connn.getRaza()-numar, this.connn.getCoordonata2()+this.connn.getRaza()/2);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.connn.getCoordonata5(), this.connn.getCoordonata6(), this.connn.getLungime(), this.connn.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata2()+this.connn.getRaza()/2);
//            }
//            else if(this.connn.getCoordonata1()<this.connn.getCoordonata5())
//            {
//                int numar=this.connn.getCoordonata5()-this.connn.getCoordonata1()-2*this.connn.getRaza();
//                int numar1=this.connn.getCoordonata5()-this.connn.getCoordonata3()+this.connn.getLungime();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.connn.getCoordonata1(), this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+2*this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1()+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata3(), this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata1(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata1()+2*this.connn.getRaza(), this.connn.getCoordonata2()+this.connn.getRaza()/2);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.connn.getCoordonata5(), this.connn.getCoordonata6(), this.connn.getLungime(), this.connn.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.connn.getCoordonata5()+this.connn.getLungime()+numar, this.connn.getCoordonata2(), 2*this.connn.getRaza(), this.connn.getRaza(), 2*this.connn.getRaza(), 2*this.connn.getRaza());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata5()+this.connn.getLungime()+numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+numar1, this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata5()+2*this.connn.getRaza()+this.connn.getLungime()+numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+numar1, this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata5()+this.connn.getLungime()+numar+this.connn.getRaza(),this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+numar1, this.connn.getCoordonata4());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.connn.getCoordonata5()+this.connn.getLungime()+numar,this.connn.getCoordonata2()+this.connn.getRaza()/2, this.connn.getCoordonata5()+2*this.connn.getRaza()+this.connn.getLungime()+numar, this.connn.getCoordonata2()+this.connn.getRaza()/2);
//            }
//        }
//
//        else if(this.nume.equals("SimetrieTrunchiCon")) {
//            if (this.trr.getCoordonata2() > this.trr.getCoordonata6()) {
//                int numar=this.trr.getCoordonata2()-this.trr.getCoordonata6();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.trr.getCoordonata5(), this.trr.getCoordonata6(), this.trr.getLungime(), this.trr.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata6()-numar, this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata6()-numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata6()-numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata6()-numar  + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata6()-numar  + this.trr.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4())+ this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata6()-numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata6()-numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
//            }
//            else if (this.trr.getCoordonata2() < this.trr.getCoordonata6()) {
//                int numar=this.trr.getCoordonata6()-this.trr.getCoordonata2();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.trr.getCoordonata5(), this.trr.getCoordonata6(), this.trr.getLungime(), this.trr.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata6()+numar, this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata6()+numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata6()+numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata6()+numar  + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata6()+numar  + this.trr.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4())+ this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata6()+numar + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata6()+numar-(this.trr.getCoordonata2()-this.trr.getCoordonata4()) + this.trr.getRaza2() / 4);
//            }
//            else if (this.trr.getCoordonata1() > this.trr.getCoordonata5()) {
//                int numar=this.trr.getCoordonata1()-this.trr.getCoordonata5()-this.trr.getLungime();
//                int numar1=this.trr.getCoordonata3()-this.trr.getCoordonata5()-this.trr.getLungime();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.trr.getCoordonata5(), this.trr.getCoordonata6(), this.trr.getLungime(), this.trr.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata5()-this.trr.getRaza1()-numar, this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata5()-this.trr.getRaza2()-numar1, this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza1()-numar, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()-this.trr.getRaza2()-numar1, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza1()-numar + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()-this.trr.getRaza2()-numar1 + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza1()-numar, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()-this.trr.getRaza1()-numar+ this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza2()-numar1 , this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata5()-this.trr.getRaza2()-numar1  + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()-this.trr.getRaza1()-numar+ this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()-this.trr.getRaza2()-numar1+ this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//            }
//            else if(this.trr.getCoordonata1()<this.trr.getCoordonata5())
//            {
//                int numar=this.trr.getCoordonata5()-this.trr.getCoordonata1()-this.trr.getRaza1();
//                int numar1=this.trr.getCoordonata5()-this.trr.getCoordonata3();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata1(), this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata3(), this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata1() + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata3(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata3() + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata1() + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata3() + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.trr.getCoordonata5(), this.trr.getCoordonata6(), this.trr.getLungime(), this.trr.getLatime());
//                ////////////////////////////////////////////////////////
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata5()+this.trr.getLungime()+numar, this.trr.getCoordonata2(), this.trr.getRaza1(), this.trr.getRaza1() / 2, this.trr.getCoordonata1(), this.trr.getCoordonata1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.trr.getCoordonata5()+numar1, this.trr.getCoordonata4(), this.trr.getRaza2(), this.trr.getRaza2() / 2, this.trr.getCoordonata3(), this.trr.getCoordonata3());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()+this.trr.getLungime()+numar, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()+numar1, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()+this.trr.getLungime()+numar+ this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()+numar1+ this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()+this.trr.getLungime()+numar, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()+this.trr.getLungime()+numar + this.trr.getRaza1(), this.trr.getCoordonata2() + this.trr.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()+numar1, this.trr.getCoordonata4() + this.trr.getRaza2() / 4, this.trr.getCoordonata5()+numar1 + this.trr.getRaza2(), this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.trr.getCoordonata5()+this.trr.getLungime()+numar + this.trr.getRaza1() / 2, this.trr.getCoordonata2() + this.trr.getRaza1() / 4, this.trr.getCoordonata5()+numar1 + this.trr.getRaza2() / 2, this.trr.getCoordonata4() + this.trr.getRaza2() / 4);
//            }
//        }
//
//        else if(this.nume.equals("SimetrieCilindru")) {
//            if (this.cil.getCoordonata2() > this.cil.getCoordonata6()) {
//                int numar=this.cil.getCoordonata2() -this.cil.getCoordonata6()-this.cil.getLatime();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.cil.getCoordonata5(), this.cil.getCoordonata6(), this.cil.getLungime(), this.cil.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata6()-numar, this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata6() -this.cil.getH()-numar, this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6()-numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata6()-this.cil.getH()-numar + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()-numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()-this.cil.getH()-numar + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6()-numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()-numar + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6() -this.cil.getH()-numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6() -this.cil.getH()-numar + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata6()-numar+this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata6()-numar-this.cil.getH() + this.cil.getRaza1() / 4);
//            }
//            else if (this.cil.getCoordonata2() < this.cil.getCoordonata6()) {
//                int numar=this.cil.getCoordonata6() -this.cil.getCoordonata2();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.cil.getCoordonata5(), this.cil.getCoordonata6(), this.cil.getLungime(), this.cil.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata6()+numar, this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata6() +this.cil.getH()+numar, this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6()+numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata6()+this.cil.getH()+numar + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()+numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()+this.cil.getH()+numar + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6()+numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6()+numar + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata6() +this.cil.getH()+numar + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata6() +this.cil.getH()+numar + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata6()+numar+this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata6()+numar+this.cil.getH() + this.cil.getRaza1() / 4);
//            }
//            else if (this.cil.getCoordonata1() > this.cil.getCoordonata5()) {
//                int numar=this.cil.getCoordonata1() -this.cil.getCoordonata5()-this.cil.getLungime();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar, this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()-this.cil.getRaza1()-numar + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.cil.getCoordonata5(), this.cil.getCoordonata6(), this.cil.getLungime(), this.cil.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//            }
//            else if (this.cil.getCoordonata1() < this.cil.getCoordonata5()) {
//                int numar=this.cil.getCoordonata5() -this.cil.getCoordonata1()-this.cil.getRaza1()+this.cil.getLungime();
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata1() + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                /////////////////////////////////////////////////////////
//                g.drawRect(this.cil.getCoordonata5(), this.cil.getCoordonata6(), this.cil.getLungime(), this.cil.getLatime());
//                ////////////////////////////////////////////////////////
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawRoundRect(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2() -this.cil.getH(), this.cil.getRaza1(), this.cil.getRaza1() / 2, this.cil.getRaza1(), this.cil.getRaza1());
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()+numar + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar + this.cil.getRaza1(), this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar + this.cil.getRaza1(), this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()+numar, this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar + this.cil.getRaza1(), this.cil.getCoordonata2() -this.cil.getH() + this.cil.getRaza1() / 4);
//
//                g.setColor(this.culoare);
//                g.drawLine(this.cil.getCoordonata5()+numar + this.cil.getRaza1() / 2, this.cil.getCoordonata2()-this.cil.getH() + this.cil.getRaza1() / 4, this.cil.getCoordonata5()+numar + this.cil.getRaza1() / 2, this.cil.getCoordonata2() + this.cil.getRaza1() / 4);
//            }
//        }
        }

        public float[][] matmul ( float[][] a, float[][] b)
        {
            int colsA = a[0].length;
            int rowsA = a.length;
            int colsB = b[0].length;
            int rowsB = b.length;

            if (colsA != rowsB) {
                System.out.println("Coloanele lui a nu se potrivesc cu randurile lui b");
                return null;
            }
            float[][] result = new float[rowsA][colsB];
            for (int i = 0; i < rowsA; i++) {
                for (int j = 0; j < colsB; j++) {
                    float sum = 0;
                    for (int k = 0; k < colsA; k++) {
                        sum += a[i][k] * b[k][j];
                    }
                    result[i][j] = sum;
                }
            }
            return result;
        }

        public float[][] vecToMatrix ( float[] v)
        {
            float[][] m = new float[3][1];
            m[0][0] = v[0];
            m[1][0] = v[1];
            m[2][0] = v[2];
            return m;
        }

        public float[] matrixToVec ( float[][] m)
        {
            float[] v = new float[3];
            v[0] = m[0][0];
            v[1] = m[1][0];
            if (m.length > 2)
                v[2] = m[2][0];
            return v;
        }

        public float[][] matmul ( float[][] a, float[] b)
        {
            float[][] m = this.vecToMatrix(b);
            return this.matmul(a, m);
        }

        public float[] matmull ( float[][] a, float[] b)
        {
            float[][] m = this.vecToMatrix(b);
            float[][] rez = this.matmul(a, m);
            float[] rezz = this.matrixToVec(rez);
            return rezz;
        }
        public void logMatrix ( float[][] m)
        {
            int cols = m[0].length;
            int rows = m.length;
            System.out.println(rows + "x" + cols);
            System.out.println("---------------------");
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    System.out.print(m[i][j] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }


