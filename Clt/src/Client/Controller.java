package Client;

import Client.FabricaCorpuri;
import Client.GUI;

import java.io.*;
import java.util.ArrayList;


public class Controller {
    private ArrayList<FabricaCorpuri> lista;
    private GUI gui;

    public Controller(ArrayList<FabricaCorpuri> lista, GUI gui) {
        this.lista=lista;
        this.gui=gui;
    }

    public boolean salvare(ArrayList<FabricaCorpuri> fabricaCorpuri) throws IOException {
        File f=new File("corpuri.txt");
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
        out.writeObject(fabricaCorpuri);
        return true;
    }
    public ArrayList<FabricaCorpuri> incarcare(FabricaCorpuri fabricaCorpuri)
    {
        lista.add(fabricaCorpuri);
        return lista;
    }
    public boolean afisare() throws IOException, ClassNotFoundException {
        File f=new File("corpuri.txt");
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
        lista=(ArrayList<FabricaCorpuri>)in.readObject();
        return true;
    }
}

