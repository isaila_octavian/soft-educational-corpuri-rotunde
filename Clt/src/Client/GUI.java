package Client;

import Client.*;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class GUI extends JFrame {

    private JFrame frame = new JFrame("Soft educational pentru corpuri rotunde");
    private JButton buton = new JButton("Cilindru");
    private JButton buton1 = new JButton("Trunchi");
    private JButton buton2 = new JButton("Con");
    private JButton buton3 = new JButton("Sfera");
    private JButton buton4 = new JButton("Cilindru");
    private JButton buton5 = new JButton("Sfera");
    private JButton buton6 = new JButton("Con");
    private JButton buton7 = new JButton("TrunchiCon");
    private JButton buton0 = new JButton("CilindruS");
    private JButton buton11 = new JButton("TrunchiS");
    private JButton buton22 = new JButton("ConS");
    private JButton buton33 = new JButton("SferaS");
    private JPanel p = new JPanel();
    private JPanel p1 = new JPanel();
    private JPanel p2 = new JPanel();
    private JPanel pp = new JPanel();
    private JPanel pp1 = new JPanel();
    private JPanel pp2 = new JPanel();
    private JPanel pp3 = new JPanel();
    private JLabel l1 = new JLabel("");
    private JLabel l2 = new JLabel("");
    private JLabel l3 = new JLabel("");
    private JLabel l4 = new JLabel("");
    private JLabel ll1 = new JLabel("X1");
    private JLabel ll2 = new JLabel("Y1");
    private JLabel ll3 = new JLabel("Z1");
    private JLabel ll4 = new JLabel("X2/H");
    private JLabel ll5 = new JLabel("Y2");
    private JLabel ll6 = new JLabel("Z2");
    private JLabel ll7 = new JLabel("Raza1");
    private JLabel ll8 = new JLabel("Raza2");
    private JLabel ll9 = new JLabel("α");
    private JLabel ll10 = new JLabel("β");
    private JLabel ll11 = new JLabel("γ");
    private JLabel ll12 = new JLabel("");
    private JLabel ll13 = new JLabel("");
    private JLabel ll14 = new JLabel("");
    private JLabel ll15 = new JLabel("X simetrie");
    private JLabel ll16 = new JLabel("Y simetrie");
    private JLabel ll17 = new JLabel("Z simetrie");
    private JLabel ll18 = new JLabel("Butoane afisare rezultate:");
    private JLabel ll19 = new JLabel("Lungime");
    private JLabel ll21 = new JLabel("Latime");
    private JLabel ll20 = new JLabel("Afisare rezultate");
    private JLabel ll25 = new JLabel("ID:");

    private JLabel l5 = new JLabel("INFO: Cilindrul va fi afisat in panelul albastru inchis, Conul in panelul portocaliu, Trunchiul de con in panelul galben, iar Sfera in panelul verde!");
    private JLabel label1 = new JLabel("FORMULE SFERA");
    private JLabel label2 = new JLabel("Aria= 4*π*R^2");
    private JLabel label3 = new JLabel("Volumul= 4*π*R^3");
    private JLabel label4 = new JLabel("FORMULE CILINDRU");
    private JLabel label5 = new JLabel("Aria bazei= π*R^2");
    private JLabel label6 = new JLabel("Aria laterala= 2*π*R*h");
    private JLabel label7 = new JLabel("Aria totala= Al+2*Ab");
    private JLabel label8 = new JLabel("Volum= π*R^2*h");
    private JLabel label9 = new JLabel("FORMULE CON");
    private JLabel label10 = new JLabel("Aria bazei= π*R^2");
    private JLabel label11 = new JLabel("Aria laterala= π*R*h");
    private JLabel label12 = new JLabel("Aria totala= Al+Ab");
    private JLabel label13 = new JLabel("Volumul= π*R^2*h");
    private JLabel label14 = new JLabel("FORMULE TRUNCHI CON");
    private JLabel label15 = new JLabel("Aria bazei mici= π*r^2");
    private JLabel label16 = new JLabel("Aria bazei mari= π*R^2");
    private JLabel label17 = new JLabel("Aria laterala= π*h*(R+r)");
    private JLabel label18 = new JLabel("Aria totala= Al+Ab+AB");
    private JLabel label19 = new JLabel("Volumul= π*h*(R^2+r^2+R*r)/3");
    private JTextField t1 = new JTextField(3);
    private JTextField t2 = new JTextField(3);
    private JTextField t3 = new JTextField(3);
    private JTextField t4 = new JTextField(3);
    private JTextField t5 = new JTextField(3);
    private JTextField t6 = new JTextField(3);
    private JTextField t7 = new JTextField(3);
    private JTextField t8 = new JTextField(3);
    private JTextField t9 = new JTextField(3);
    private JTextField t10 = new JTextField(3);
    private JTextField t11 = new JTextField(3);
    private JTextField t12 = new JTextField(3);
    private JTextField t13 = new JTextField(3);
    private JTextField t14 = new JTextField(3);
    private JTextField t15 = new JTextField(3);
    private JTextField t16 = new JTextField(3);
    private JTextField t20 = new JTextField(3);
    private JTextField t21 = new JTextField(3);
    private JMenuBar bar = new JMenuBar();
    private JMenu file = new JMenu("Fisier");
    private JMenuItem salvare = new JMenuItem("Salvare");
    private JMenuItem deschide = new JMenuItem("Deschide");
    //private ArrayList<Corpuri> lista = new ArrayList<Corpuri>();
    private int cilindrunr;
    private int connr;
    private int trunchinr;
    private int sferanr;
    private JLabel label20=new JLabel("SELECTEAZA LIMBA");
    private JButton romania;
    private JButton usa;
    private JButton germania;
    private boolean ok1;
    private boolean ok2;
    private boolean ok3;
    private boolean ok4;


    public GUI() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor<Conexiune> constructor = Conexiune.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        Conexiune foo = constructor.newInstance();

        this.ok1=false;
        this.ok2=false;
        this.ok3=false;
        this.ok3=false;

        frame.setLayout(null);
        frame.setSize(1500, 830);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setBounds(10, 0, 1500, 830);

        p.setLayout(null);
        p.setBounds(0, 0, 1500, 140);
        p.setBackground(Color.CYAN);

        bar.setBounds(0, 0, 1500, 25);

        buton.setBounds(10, 40, 90, 25);
        buton.setBackground(Color.GREEN);
        buton0.setBounds(10, 80, 90, 25);
        buton0.setBackground(Color.GREEN);
        buton1.setBounds(105, 40, 90, 25);
        buton1.setBackground(Color.GREEN);
        buton11.setBounds(105, 80, 90, 25);
        buton11.setBackground(Color.GREEN);
        buton2.setBounds(200, 40, 70, 25);
        buton2.setBackground(Color.GREEN);
        buton22.setBounds(200, 80, 70, 25);
        buton22.setBackground(Color.GREEN);
        buton3.setBounds(275, 40, 80, 25);
        buton3.setBackground(Color.GREEN);
        buton33.setBounds(275, 80, 80, 25);
        buton33.setBackground(Color.GREEN);

        buton4.setBounds(950, 55, 120, 25);
        buton4.setBackground(Color.GREEN);
        buton5.setBounds(1085, 55, 120, 25);
        buton5.setBackground(Color.GREEN);
        buton6.setBounds(950, 90, 120, 25);
        buton6.setBackground(Color.GREEN);
        buton7.setBounds(1085, 90, 120, 25);
        buton7.setBackground(Color.GREEN);

        ll1.setBounds(390, 20, 20, 25);
        t1.setBounds(380, 40, 35, 25);
        ll2.setBounds(430, 20, 20, 25);
        t2.setBounds(420, 40, 35, 25);
        ll3.setBounds(470, 20, 20, 25);
        t3.setBounds(460, 40, 35, 25);
        ll4.setBounds(383, 65, 30, 25);
        t4.setBounds(380, 85, 35, 25);
        ll5.setBounds(430, 65, 20, 25);
        t5.setBounds(420, 85, 35, 25);
        ll6.setBounds(470, 65, 20, 25);
        t6.setBounds(460, 85, 35, 25);
        ll7.setBounds(500, 20, 60, 25);
        t7.setBounds(500, 40, 35, 25);
        ll8.setBounds(500, 65, 60, 25);
        t8.setBounds(500, 85, 35, 25);
        ll9.setBounds(590, 20, 20, 25);
        t9.setBounds(580, 40, 35, 25);
        ll10.setBounds(630, 20, 20, 25);
        t10.setBounds(620, 40, 35, 25);
        ll11.setBounds(670, 20, 20, 25);
        t11.setBounds(660, 40, 35, 25);
        ll12.setBounds(5, 0, 260, 25);
        ll13.setBounds(5, 20, 260, 25);
        ll14.setBounds(5, 40, 260, 25);
        ll20.setBounds(75, 30, 260, 25);
        l5.setBounds(10, 110, 1000, 25);
        ll15.setBounds(730, 20, 60, 25);
        t12.setBounds(740, 40, 35, 25);
        ll16.setBounds(795, 20, 60, 25);
        t13.setBounds(805, 40, 35, 25);
        ll17.setBounds(860, 20, 60, 25);
        t14.setBounds(870, 40, 35, 25);
        ll19.setBounds(740, 65, 60, 25);
        ll18.setBounds(980, 20, 150, 25);
        t15.setBounds(750, 85, 35, 25);
        ll21.setBounds(820, 65, 60, 25);
        t16.setBounds(825, 85, 35, 25);
        t21.setBounds(620, 80, 35, 25);
        ll25.setBounds(600, 80, 20, 25);


        pp.setLayout(null);
        l1.setBounds(1, 0, 150, 25);
        pp.add(l1);
        pp.setBounds(10, 145, 635, 320);
        pp.setBackground(Color.BLUE);
        pp.setVisible(true);

        pp1.setLayout(null);
        l2.setBounds(1, 0, 180, 25);
        pp1.add(l2);
        pp1.setBounds(655, 145, 635, 320);
        pp1.setBackground(Color.YELLOW);
        pp1.setVisible(true);

        pp2.setLayout(null);
        l3.setBounds(1, 0, 150, 25);
        pp2.add(l3);
        pp2.setBounds(10, 470, 635, 320);
        pp2.setBackground(Color.ORANGE);
        pp2.setVisible(true);

        pp3.setLayout(null);
        l4.setBounds(1, 0, 150, 25);
        pp3.add(l4);
        pp3.setBounds(655, 470, 635, 320);
        pp3.setBackground(Color.GREEN);
        pp3.setVisible(true);

        p2.setLayout(null);
        p2.setBackground(Color.BLUE);
        p2.setBounds(1220, 32, 260, 90);
        p2.add(ll12);
        p2.add(ll13);
        p2.add(ll14);
        p2.add(ll20);

        p1.setLayout(null);
        p1.setBounds(1300, 125, 200, 830);
        p1.setBackground(Color.CYAN);

        ImageIcon iconAnglia=new ImageIcon("C:\\Users\\User\\IdeaProjects\\Clt\\anglia.jpg");
        ImageIcon iconRomania=new ImageIcon("C:\\Users\\User\\IdeaProjects\\Clt\\romaniaa.png");
        ImageIcon iconGermania=new ImageIcon("C:\\Users\\User\\IdeaProjects\\Clt\\germania.png");

        romania=new JButton(iconRomania);
        usa=new JButton(iconAnglia);
        germania=new JButton(iconGermania);

        label1.setBounds(30, 10, 200, 15);
        label2.setBounds(10, 35, 200, 15);
        label3.setBounds(10, 60, 200, 15);
        label4.setBounds(30, 110, 200, 15);
        label5.setBounds(10, 130, 200, 15);
        label6.setBounds(10, 160, 200, 15);
        label7.setBounds(10, 185, 200, 15);
        label8.setBounds(10, 210, 200, 15);
        label9.setBounds(30, 260, 200, 15);
        label10.setBounds(10, 285, 200, 15);
        label11.setBounds(10, 310, 200, 15);
        label12.setBounds(10, 335, 200, 15);
        label13.setBounds(10, 360, 200, 15);
        label14.setBounds(30, 410, 200, 15);
        label15.setBounds(10, 435, 200, 15);
        label16.setBounds(10, 460, 200, 15);
        label17.setBounds(10, 485, 200, 15);
        label18.setBounds(10, 510, 200, 15);
        label19.setBounds(10, 535, 200, 15);
        label20.setBounds(35,570,200,15);
        romania.setBounds(10,590,50,70);
        usa.setBounds(70,590,50,70);
        germania.setBounds(130,590,50,70);

        p1.add(label1);
        p1.add(label2);
        p1.add(label3);
        p1.add(label4);
        p1.add(label5);
        p1.add(label6);
        p1.add(label7);
        p1.add(label8);
        p1.add(label9);
        p1.add(label10);
        p1.add(label11);
        p1.add(label12);
        p1.add(label13);
        p1.add(label14);
        p1.add(label15);
        p1.add(label16);
        p1.add(label17);
        p1.add(label18);
        p1.add(label19);
        p1.add(label20);
        p1.add(romania);
        p1.add(usa);
        p1.add(germania);


        frame.add(p1);
        frame.add(p2);
        frame.add(pp);
        frame.add(pp1);
        frame.add(pp2);
        frame.add(pp3);
        frame.add(p);

        p.add(bar);
        bar.add(file);
        file.add(salvare);
        file.add(deschide);
        p.add(buton);
        p.add(buton1);
        p.add(buton2);
        p.add(buton3);
        p.add(buton4);
        p.add(buton5);
        p.add(buton6);
        p.add(buton7);
        p.add(buton0);
        p.add(buton11);
        p.add(buton22);
        p.add(buton33);

        p.add(ll1);
        p.add(t1);
        p.add(ll2);
        p.add(t2);
        p.add(ll3);
        p.add(t3);
        p.add(ll4);
        p.add(t4);
        p.add(ll5);
        p.add(t5);
        p.add(ll6);
        p.add(t6);
        p.add(ll7);
        p.add(t7);
        p.add(ll8);
        p.add(t8);
        p.add(ll9);
        p.add(t9);
        p.add(ll10);
        p.add(ll15);
        p.add(ll16);
        p.add(ll17);
        p.add(ll18);
        p.add(ll19);
        p.add(ll21);
        p.add(t10);
        p.add(t11);
        p.add(t12);
        p.add(t13);
        p.add(t14);
        p.add(t15);
        p.add(t16);
        p.add(t21);
        p.add(ll25);
        p.add(ll11);
        p.add(l5);

        String hostname = "localhost";
        int port = 9091;

        String[] fisier1=new String[55];
        String[] fisier2=new String[55];
        String[] fisier3=new String[55];
        String limba1="romana";
        String limba2="engleza";
        String limba3="germana";
        Language language=new Language(limba1);
        Observer ob1=new Observer(limba1,language);
        Observer ob2=new Observer(limba2,language);
        Observer ob3=new Observer(limba3,language);

        try (Socket socket = new Socket(hostname, port)) {

            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);


            do {
                buton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        Object sursa = e.getSource();
                        if (sursa == buton) {
                            if (t1.getText().equals("") || t2.getText().equals("") || t4.getText().equals("") || t3.getText().equals("") || t7.getText().equals("") || t9.getText().equals("") || t10.getText().equals("") || t11.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                ok1=true;
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t3.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t4.getText();
                                int nr4 = Integer.parseInt(s4);
                                String s5 = t7.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t9.getText();
                                double nr6 = Double.parseDouble(s6);
                                String s7 = t10.getText();
                                double nr7 = Double.parseDouble(s7);
                                String s8 = t11.getText();
                                double nr8 = Double.parseDouble(s8);
                                String id = t21.getText();
                                int idd = Integer.parseInt(id);
                                System.out.println("Aici");
                                Color ccc = Color.RED;
                                    //ccc = JColorChooser.showDialog(null, "Alege culoarea pentru Cilindru", ccc);
                                if(language.getLanguage().equals("romana"))
                                {
                                    ccc = JColorChooser.showDialog(null, "fisier1[47]", ccc);
                                }else if(language.getLanguage().equals("engleza"))
                                {
                                    ccc = JColorChooser.showDialog(null, fisier2[47], ccc);
                                }else if(language.getLanguage().equals("germana"))
                                {
                                    ccc = JColorChooser.showDialog(null, fisier3[47], ccc);
                                }

                                if (ccc == null)
                                    ccc = (Color.RED);
                                FabricaDesenare cilindru = new FabricaDesenare("Cilindru", ccc, nr1, nr2, nr3, nr4, nr5, nr6, nr7, nr8);

                                float[][] rotationZ = {
                                        {(float) Math.cos(nr8), (float) -Math.sin(nr8), 0},
                                        {(float) Math.sin(nr8), (float) Math.cos(nr8), 0},
                                        {0, 0, 1}
                                };

                                float[][] rotationX = {
                                        {1, 0, 0},
                                        {0, (float) Math.cos(nr6), (float) -Math.sin(nr6)},
                                        {0, (float) Math.sin(nr6), (float) Math.cos(nr6)}
                                };

                                float[][] rotationY = {
                                        {(float) Math.cos(nr7), 0, (float) Math.sin(nr7)},
                                        {0, 1, 0},
                                        {(float) -Math.sin(nr7), 0, (float) Math.cos(nr7)}
                                };
                                cilindru.logMatrix(rotationX);
                                cilindru.logMatrix(rotationY);
                                cilindru.logMatrix(rotationZ);

                                float[][] projection = {
                                        {1, 0, 0},
                                        {0, 1, 0}
                                };

                                float[] point = new float[4];
                                point = new float[]{(float) nr1, (float) nr2, (float) nr3};

                                float[] rot = cilindru.matmull(rotationZ, point);
                                float[] rot1 = cilindru.matmull(rotationY, rot);
                                float[] rot2 = cilindru.matmull(rotationX, rot1);
                                float[] vec = cilindru.matmull(projection, rot2);
                                System.out.println(vec[0]);
                                System.out.println(vec[1]);

                                FabricaDesenare cilindru1 = new FabricaDesenare("Cilindru", ccc, (int) vec[0], (int) vec[1], nr4, nr5);

                                //FabricaCorpuri corp = new FabricaCorpuri("Cilindru", cilindru1, ccc);
                                cilindru1.setCuloare(ccc);

                                cilindru1.setSize(650, 410);
                                //lista.add(cilindru1);

                                pp.add(cilindru1);
                                pp.repaint();

                                Color col = cilindru1.getCuloare();
                                String st=String.valueOf(col.getRGB());

                                try {
                                    foo.theQuery("INSERT INTO cilindru (id,nume,culoare,coordonata1,coordonata2,h,raza1) VALUES ('" + idd + "','" + "Cilindru" + "','" + st + "','" + (int) vec[0] + "','" + (int) vec[1] + "','" + nr4 + "','" + nr5 + "')");
                                } catch (Exception ex) {
                                    System.out.println(ex.getMessage());
                                }

                                writer.println("Cilindru");

                            }
                        }

                    }
                });

                ////////////////
                buton1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ee) {

                        Object sursa = ee.getSource();
                        if (sursa == buton1) {
                            if (t1.getText().equals("") || t2.getText().equals("") || t3.getText().equals("") || t4.getText().equals("") || t5.getText().equals("") || t6.getText().equals("") || t7.getText().equals("") || t8.getText().equals("") || t9.getText().equals("") || t10.getText().equals("") || t11.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                ok2=true;
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t3.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t4.getText();
                                int nr4 = Integer.parseInt(s4);
                                String s5 = t5.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t6.getText();
                                int nr6 = Integer.parseInt(s6);
                                String s7 = t7.getText();
                                int nr7 = Integer.parseInt(s7);
                                String s8 = t8.getText();
                                int nr8 = Integer.parseInt(s8);
                                String s9 = t9.getText();
                                double nr9 = Double.parseDouble(s9);
                                String s10 = t10.getText();
                                double nr10 = Double.parseDouble(s10);
                                String s11 = t11.getText();
                                double nr11 = Double.parseDouble(s11);
                                Color ccc = Color.RED;
                                String id = t21.getText();
                                int idd = Integer.parseInt(id);

                                //ccc = JColorChooser.showDialog(null, "Alege culoare pentru trunchiul de con", ccc);
                                    if(language.getLanguage().equals("romana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier1[48], ccc);
                                    }else if(language.getLanguage().equals("engleza"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier2[48], ccc);
                                    }else if(language.getLanguage().equals("germana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier3[48], ccc);
                                    }
                                if (ccc == null)
                                    ccc = (Color.RED);
                                FabricaDesenare trunchiCon = new FabricaDesenare("TrunchiCon", ccc, nr1, nr2, nr3, nr4, nr5, nr6, nr7, nr8, nr9, nr10, nr11);
                                float[][] rotationZ = {
                                        {(float) Math.cos(nr11), (float) -Math.sin(nr11), 0},
                                        {(float) Math.sin(nr11), (float) Math.cos(nr11), 0},
                                        {0, 0, 1}
                                };

                                float[][] rotationX = {
                                        {1, 0, 0},
                                        {0, (float) Math.cos(nr9), (float) -Math.sin(nr9)},
                                        {0, (float) Math.sin(nr9), (float) Math.cos(nr9)}
                                };

                                float[][] rotationY = {
                                        {(float) Math.cos(nr10), 0, (float) Math.sin(nr10)},
                                        {0, 1, 0},
                                        {(float) -Math.sin(nr10), 0, (float) Math.cos(nr10)}
                                };

                                float[][] projection = {
                                        {1, 0, 0},
                                        {0, 1, 0}
                                };

                                float[] point = new float[4];
                                point = new float[]{(float) nr1, (float) nr2, (float) nr3};

                                float[] rot = trunchiCon.matmull(rotationZ, point);
                                float[] rot1 = trunchiCon.matmull(rotationY, rot);
                                float[] rot2 = trunchiCon.matmull(rotationX, rot1);
                                float[] vec = trunchiCon.matmull(projection, rot2);

                                float[] point1 = new float[4];
                                point1 = new float[]{(float) nr4, (float) nr5, (float) nr6};

                                float[] rot0 = trunchiCon.matmull(rotationZ, point1);
                                float[] rot11 = trunchiCon.matmull(rotationY, rot0);
                                float[] rot22 = trunchiCon.matmull(rotationX, rot11);
                                float[] vec1 = trunchiCon.matmull(projection, rot22);

                                FabricaDesenare trunchiCon1 = new FabricaDesenare("TrunchiCon", ccc, (int) vec[0], (int) vec[1], (int) vec1[0], (int) vec1[1], nr7, nr8);
                                //FabricaCorpuri corp1 = new FabricaCorpuri("TrunchiCon", trunchiCon1, Color.RED);
                                trunchiCon1.setCuloare(ccc);

                                trunchiCon1.setBackground(Color.BLACK);
                                trunchiCon1.setSize(650, 410);
                                //lista.add(trunchiCon1);

                                pp1.add(trunchiCon1);
                                pp1.repaint();

                                Color col = trunchiCon1.getCuloare();
                                String st=String.valueOf(col.getRGB());

                                try {
                                    foo.theQuery("INSERT INTO trunchicon(id,nume,culoare,coordonata1,coordonata2,coordonata3,coordonata4,raza1,raza2) VALUES ('" + idd + "','" + "TrunchiCon" + "','" + st + "','" + (int) vec[0] + "','" + (int) vec[1] + "','" + (int) vec1[0] + "','" + (int) vec1[1] + "','" + nr7 + "','" + nr8 + "')");
                                } catch (Exception ex) {
                                    System.out.println(ex.getMessage());
                                }

                                writer.println("TrunchiCon");
                            }

                        }
                    }
                });
                ////////////////
                buton2.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        Object sursa = e.getSource();
                        if (sursa == buton2) {
                            if (t1.getText().equals("") || t2.getText().equals("") || t3.getText().equals("") || t4.getText().equals("") || t5.getText().equals("") || t6.getText().equals("") || t7.getText().equals("") || t9.getText().equals("") || t10.getText().equals("") || t11.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                ok3=true;
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t3.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t4.getText();
                                int nr4 = Integer.parseInt(s4);
                                String s5 = t5.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t6.getText();
                                int nr6 = Integer.parseInt(s6);
                                String s7 = t7.getText();
                                int nr7 = Integer.parseInt(s7);
                                String s8 = t9.getText();
                                double nr8 = Double.parseDouble(s8);
                                String s9 = t10.getText();
                                double nr9 = Double.parseDouble(s9);
                                String s10 = t11.getText();
                                double nr10 = Double.parseDouble(s10);
                                String id = t21.getText();
                                int idd = Integer.parseInt(id);

                                Color ccc = Color.RED;
                                //ccc = JColorChooser.showDialog(null, "Alege culoare pentru con", ccc);
                                    if(language.getLanguage().equals("romana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier1[49], ccc);
                                    }else if(language.getLanguage().equals("engleza"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier2[49], ccc);
                                    }else if(language.getLanguage().equals("germana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier3[49], ccc);
                                    }
                                if (ccc == null)
                                    ccc = (Color.RED);
                                FabricaDesenare conn = new FabricaDesenare("Con", ccc, nr1, nr2, nr3, nr4, nr5, nr6, nr7, nr8, nr9, nr10);
                                float[][] rotationZ = {
                                        {(float) Math.cos(nr10), (float) -Math.sin(nr10), 0},
                                        {(float) Math.sin(nr10), (float) Math.cos(nr10), 0},
                                        {0, 0, 1}
                                };

                                float[][] rotationX = {
                                        {1, 0, 0},
                                        {0, (float) Math.cos(nr8), (float) -Math.sin(nr8)},
                                        {0, (float) Math.sin(nr8), (float) Math.cos(nr8)}
                                };

                                float[][] rotationY = {
                                        {(float) Math.cos(nr9), 0, (float) Math.sin(nr9)},
                                        {0, 1, 0},
                                        {(float) -Math.sin(nr9), 0, (float) Math.cos(nr9)}
                                };

                                float[][] projection = {
                                        {1, 0, 0},
                                        {0, 1, 0}
                                };

                                float[] point = new float[4];
                                point = new float[]{(float) nr1, (float) nr2, (float) nr3};

                                float[] rot = conn.matmull(rotationZ, point);
                                float[] rot1 = conn.matmull(rotationY, rot);
                                float[] rot2 = conn.matmull(rotationX, rot1);
                                float[] vec = conn.matmull(projection, rot2);

                                float[] point1 = new float[4];
                                point1 = new float[]{(float) nr4, (float) nr5, (float) nr6};

                                float[] rot0 = conn.matmull(rotationZ, point1);
                                float[] rot11 = conn.matmull(rotationY, rot0);
                                float[] rot22 = conn.matmull(rotationX, rot11);
                                float[] vec1 = conn.matmull(projection, rot22);

                                FabricaDesenare conn1 = new FabricaDesenare("Con", ccc, (int) vec[0], (int) vec[1], (int) vec1[0], (int) vec1[1], nr7);
                                //FabricaCorpuri corp = new FabricaCorpuri("Con", conn1, ccc);
                                conn1.setCuloare(ccc);

                                conn1.setSize(650, 410);
                                //lista.add(conn1);

                                pp2.add(conn1);
                                pp2.repaint();

                                Color col = conn1.getCuloare();
                                String st=String.valueOf(col.getRGB());

                                try {
                                    foo.theQuery("INSERT INTO con(id,nume,culoare,coordonata1,coordonata2,coordonata3,coordonata4,raza1) VALUES ('" + idd + "','" + "Con" + "','" + st + "','" + (int) vec[0] + "','" + (int) vec[1] + "','" + (int) vec1[0] + "','" + (int) vec1[1] + "','" + nr7 + "')");
                                } catch (Exception ex) {
                                    System.out.println(ex.getMessage());
                                }

                                writer.println("Con");
                            }
                        }
                    }
                });
                ///////////////
                buton3.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        Object sursa = e.getSource();
                        if (sursa == buton3) {
                            if (t1.getText().equals("") || t2.getText().equals("") || t7.getText().equals("") || t3.getText().equals("") || t9.getText().equals("") || t10.getText().equals("") || t11.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                ok4=true;
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t3.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s5 = t7.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t9.getText();
                                double nr6 = Double.parseDouble(s6);
                                String s7 = t10.getText();
                                double nr7 = Double.parseDouble(s7);
                                String s8 = t11.getText();
                                double nr8 = Double.parseDouble(s8);
                                Color ccc = Color.RED;
                                String id = t21.getText();
                                int idd = Integer.parseInt(id);

                                //ccc = JColorChooser.showDialog(null, "Alege culoare pentru sfera", ccc);
                                    if(language.getLanguage().equals("romana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier1[50], ccc);
                                    }else if(language.getLanguage().equals("engleza"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier2[50], ccc);
                                    }else if(language.getLanguage().equals("germana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier3[50], ccc);
                                    }
                                if (ccc == null)
                                    ccc = (Color.RED);
                                FabricaDesenare sfera = new FabricaDesenare("Sfera", ccc, nr1, nr2, nr3, nr5, nr6, nr7, nr8);
                                float[][] rotationZ = {
                                        {(float) Math.cos(nr8), (float) -Math.sin(nr8), 0},
                                        {(float) Math.sin(nr8), (float) Math.cos(nr8), 0},
                                        {0, 0, 1}
                                };

                                float[][] rotationX = {
                                        {1, 0, 0},
                                        {0, (float) Math.cos(nr6), (float) -Math.sin(nr6)},
                                        {0, (float) Math.sin(nr6), (float) Math.cos(nr6)}
                                };

                                float[][] rotationY = {
                                        {(float) Math.cos(nr7), 0, (float) Math.sin(nr7)},
                                        {0, 1, 0},
                                        {(float) -Math.sin(nr7), 0, (float) Math.cos(nr7)}
                                };

                                float[][] projection = {
                                        {1, 0, 0},
                                        {0, 1, 0}
                                };

                                float[] point = new float[4];
                                point = new float[]{(float) nr1, (float) nr2, (float) nr3};

                                float[] rot = sfera.matmull(rotationZ, point);
                                float[] rot1 = sfera.matmull(rotationY, rot);
                                float[] rot2 = sfera.matmull(rotationX, rot1);
                                float[] vec = sfera.matmull(projection, rot2);

                                FabricaDesenare sfera1 = new FabricaDesenare("Sfera", ccc, (int) vec[0], (int) vec[1], nr5);
                                //FabricaCorpuri corp = new FabricaCorpuri("Sfera", sfera1, ccc);
                                sfera1.setCuloare(ccc);

                                sfera1.setSize(650, 410);
                                //lista.add(sfera1);

                                pp3.add(sfera1);
                                pp3.repaint();

                                Color col = sfera1.getCuloare();
                                String st=String.valueOf(col.getRGB());

                                try {
                                    foo.theQuery("INSERT INTO sfera(id,nume,culoare,coordonata1,coordonata2,raza1) VALUES ('" + idd + "','" + "Sfera" + "','" + st + "','" + (int) vec[0] + "','" + (int) vec[1] + "','" + nr5 + "')");
                                } catch (Exception ex) {
                                    System.out.println(ex.getMessage());
                                }

                                writer.println("Sfera");
                            }
                        }
                    }
                });
                ///////////////
                buton6.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == buton6) {
                            if (t1.getText().equals("") || t3.getText().equals("") || t4.getText().equals("") || t6.getText().equals("") || t7.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t3.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t4.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t6.getText();
                                int nr4 = Integer.parseInt(s4);
                                String s5 = t7.getText();
                                int nr5 = Integer.parseInt(s5);
                                Color ccc = Color.RED;
                                double suma;
                                suma = Math.PI * nr5 * nr5;//b
                                double suma1;
                                suma1 = Math.PI * nr3 * nr5;//l
                                double suma2;
                                suma2 = suma1 + suma;//t
                                double suma3;
                                suma3 = Math.PI * nr3 * nr5 * nr5 / 3;//v

                                ll12.setText("Ab= " + (int) suma + " " + "Al= " + (int) suma1);
                                ll13.setText("At= " + (int) suma2);
                                ll14.setText("Vol= " + (int) suma3);
                                p2.add(ll12);
                                p2.add(ll13);
                                p2.add(ll14);
                                ll20.setText("");
                                p2.repaint();
                            }
                        }
                    }
                });
                ///////////////
                buton5.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == buton5) {
                            if (t1.getText().equals("") || t2.getText().equals("") || t7.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t7.getText();
                                int nr3 = Integer.parseInt(s3);
                                double suma;
                                suma = 4 * Math.PI * nr3 * nr3;
                                double suma1;
                                suma1 = 4 * Math.PI * nr3 * nr3 * nr3;
                                ll12.setText("A= " + (int) suma);
                                ll13.setText("Vol= " + (int) suma1);
                                ll14.setText("");
                                p2.add(ll12);
                                p2.add(ll13);
                                p2.repaint();
                            }
                        }
                    }
                });
                ///////////////
                buton4.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == buton4) {
                            if (t1.getText().equals("") || t2.getText().equals("") || t4.getText().equals("") || t7.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t4.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t7.getText();
                                int nr4 = Integer.parseInt(s4);
                                double suma;
                                suma = 2 * Math.PI * nr4 * nr4;
                                double suma1;
                                suma1 = 2 * Math.PI * nr4 * nr3;
                                double suma2;
                                suma2 = suma + suma1;
                                double suma3;
                                suma3 = Math.PI * nr4 * nr4 * nr3;


                                ll12.setText("Ab= " + (int) suma + " " + "Al= " + (int) suma1);
                                ll13.setText("At= " + (int) suma2);
                                ll14.setText("Vol= " + (int) suma3);
                                p2.add(ll12);
                                p2.add(ll13);
                                p2.add(ll14);
                                ll20.setText("");
                                p2.repaint();
                            }
                        }
                    }
                });
                //////////////
                buton7.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == buton7) {
                            if (t1.getText().equals("") || t3.getText().equals("") || t4.getText().equals("") || t6.getText().equals("") || t7.getText().equals("") || t8.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t3.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t4.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t6.getText();
                                int nr4 = Integer.parseInt(s4);
                                String s5 = t7.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t8.getText();
                                int nr6 = Integer.parseInt(s6);
                                Color ccc = Color.RED;
                                double suma;
                                suma = Math.PI * nr6 * nr6 + Math.PI * nr5 * nr5;//b
                                double suma1;
                                suma1 = Math.PI * nr3 * (nr5 + nr6);//l
                                double suma2;
                                suma2 = suma1 + suma;//t
                                double suma3;
                                suma3 = Math.PI * nr3 * (nr5 * nr6 + nr6 * nr6 + nr5 * nr5) / 3;//v

                                ll12.setText("Ab= " + (int) suma + " " + "Al= " + (int) suma1);
                                ll13.setText("At= " + (int) suma2);
                                ll14.setText("Vol= " + (int) suma3);
                                p2.add(ll12);
                                p2.add(ll13);
                                p2.add(ll14);
                                ll20.setText("");
                                p2.repaint();
                            }
                        }
                    }
                });
                //////////////
                deschide.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == deschide) {
                            String ss = t21.getText();
                            int nr = Integer.parseInt(ss);
                            String query = "select * from cilindru where id = '" + cilindrunr + "'"+"order by id DESC";
                            String query1 = "select * from trunchicon where id = '" + trunchinr + "'"+"order by id DESC";
                            String query2 = "select * from con where id = '" + connr + "'"+"order by id DESC";
                            String query3 = "select * from sfera where id = '" + sferanr + "'"+"order by id DESC";
                            try {
                                if(ok1==true) {
                                    ResultSet rs= foo.getSt().executeQuery(query);
                                    String nume = null;
                                    String culoare=null;
                                    int id=0;
                                    int coordonata1=0;
                                    int coordonata2=0;
                                    int coordonata3=0;
                                    int coordonata4=0;
                                    int h=0;
                                    int raza1=0;
                                    int raza2=0;
                                    if(rs.next())
                                    {
                                        id=rs.getInt("id");
                                        nume=rs.getString("nume");
                                        culoare =rs.getString("culoare");
                                        coordonata1 = rs.getInt("coordonata1");
                                        coordonata2 = rs.getInt("coordonata2");
                                        h = rs.getInt("h");
                                        raza1 = rs.getInt("raza1");
                                    }
                                    Color c = new Color(Integer.parseInt(culoare));
                                    //FabricaDesenare fab = foo.getQuery(query, nr);
                                    FabricaDesenare fab=new FabricaDesenare(nume,c,coordonata1,coordonata2,h,raza1);
                                    fab.setSize(650, 410);

                                    pp.add(fab);
                                    pp.repaint();
                                }

                                if(ok2==true) {
                                    ResultSet rs= foo.getSt().executeQuery(query1);
                                    String nume = null;
                                    String culoare=null;
                                    int id=0;
                                    int coordonata1=0;
                                    int coordonata2=0;
                                    int coordonata3=0;
                                    int coordonata4=0;
                                    int h=0;
                                    int raza1=0;
                                    int raza2=0;
                                    if(rs.next())
                                    {
                                        id=rs.getInt("id");
                                        nume=rs.getString("nume");
                                        culoare =rs.getString("culoare");
                                        coordonata1 = rs.getInt("coordonata1");
                                        coordonata2 = rs.getInt("coordonata2");
                                        coordonata3 = rs.getInt("coordonata3");
                                        coordonata4 = rs.getInt("coordonata4");
                                        raza1 = rs.getInt("raza1");
                                        raza2 = rs.getInt("raza2");
                                    }
                                    Color c = new Color(Integer.parseInt(culoare));
                                    FabricaDesenare fab1=new FabricaDesenare(nume,c,coordonata1,coordonata2,coordonata3,coordonata4,raza1,raza2);
                                    //FabricaDesenare fab1 = foo.getQuery(query1, nr);
                                    fab1.setSize(650, 410);

                                    pp1.add(fab1);
                                    pp1.repaint();
                                }

                                if(ok1==true) {
                                    ResultSet rs= foo.getSt().executeQuery(query2);
                                    String nume = null;
                                    String culoare=null;
                                    int id=0;
                                    int coordonata1=0;
                                    int coordonata2=0;
                                    int coordonata3=0;
                                    int coordonata4=0;
                                    int h=0;
                                    int raza1=0;
                                    int raza2=0;
                                    if(rs.next())
                                    {
                                        id=rs.getInt("id");
                                        nume=rs.getString("nume");
                                        culoare =rs.getString("culoare");
                                        coordonata1 = rs.getInt("coordonata1");
                                        coordonata2 = rs.getInt("coordonata2");
                                        coordonata3 = rs.getInt("coordonata3");
                                        coordonata4 = rs.getInt("coordonata4");
                                        raza1 = rs.getInt("raza1");
                                    }
                                    Color c = new Color(Integer.parseInt(culoare));
                                    FabricaDesenare fab2=new FabricaDesenare(nume,c,coordonata1,coordonata2,coordonata3,coordonata4,raza1);
                                    //FabricaDesenare fab2 = foo.getQuery(query2, nr);
                                    fab2.setSize(650, 410);

                                    pp2.add(fab2);
                                    pp2.repaint();
                                }

                                if(ok1==true) {
                                    ResultSet rs= foo.getSt().executeQuery(query3);
                                    String nume = null;
                                    String culoare=null;
                                    int id=0;
                                    int coordonata1=0;
                                    int coordonata2=0;
                                    int coordonata3=0;
                                    int coordonata4=0;
                                    int h=0;
                                    int raza1=0;
                                    int raza2=0;
                                    if(rs.next())
                                    {
                                        id=rs.getInt("id");
                                        nume=rs.getString("nume");
                                        culoare =rs.getString("culoare");
                                        coordonata1 = rs.getInt("coordonata1");
                                        coordonata2 = rs.getInt("coordonata2");
                                        raza1 = rs.getInt("raza1");
                                    }
                                    Color c = new Color(Integer.parseInt(culoare));
                                    FabricaDesenare fab3=new FabricaDesenare(nume,c,coordonata1,coordonata2,raza1);
                                    //FabricaDesenare fab3 = foo.getQuery(query3, nr);
                                    fab3.setSize(650, 410);

                                    pp3.add(fab3);
                                    pp3.repaint();
                                }

                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }

                });
                //////////////
                salvare.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == salvare) {
                            String query = "select id from cilindru order by id DESC";
                            String query1 = "select id from trunchicon order by id DESC";
                            String query2 = "select id from con order by id DESC";
                            String query3 = "select id from sfera order by id DESC";

                            try {

                                if(ok1==true)
                                {
                                    int id=0;
                                    ResultSet rs= foo.getSt().executeQuery(query);
                                    if(rs.next())
                                    {
                                        id=rs.getInt("id");
                                    }
                                    //cilindrunr = foo.getQueryID(query);
                                    cilindrunr=id;
                                }
                                if(ok2==true)
                                {
                                    //trunchinr = foo.getQueryID(query1);
                                    int id=0;
                                    ResultSet rs= foo.getSt().executeQuery(query1);
                                    if(rs.next())
                                    {
                                        id=rs.getInt("id");
                                    }
                                    //cilindrunr = foo.getQueryID(query);
                                    trunchinr=id;
                                }
                                if(ok3==true)
                                {
                                    //connr = foo.getQueryID(query2);
                                    int id=0;
                                    ResultSet rs= foo.getSt().executeQuery(query2);
                                    if(rs.next())
                                    {
                                        id=rs.getInt("id");
                                    }
                                    //cilindrunr = foo.getQueryID(query);
                                    connr=id;
                                }
                                if(ok4==true) {
                                    //sferanr = foo.getQueryID(query3);
                                    int id=0;
                                    ResultSet rs= foo.getSt().executeQuery(query3);
                                    if(rs.next())
                                    {
                                        id=rs.getInt("id");
                                    }
                                    //cilindrunr = foo.getQueryID(query);
                                    sferanr=id;
                                }

                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                });
                //////////////
                buton0.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == buton0) {
                            if (t1.getText().equals("") || t2.getText().equals("") || t3.getText().equals("") || t4.getText().equals("") || t7.getText().equals("") || t9.getText().equals("") || t10.getText().equals("") || t11.getText().equals("") || t12.getText().equals("") || t13.getText().equals("") || t14.getText().equals("") || t15.getText().equals("") || t16.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t3.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t4.getText();
                                int nr4 = Integer.parseInt(s4);
                                String s5 = t7.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t9.getText();
                                double nr6 = Double.parseDouble(s6);
                                String s7 = t10.getText();
                                double nr7 = Double.parseDouble(s7);
                                String s8 = t11.getText();
                                double nr8 = Double.parseDouble(s8);
                                String s9 = t12.getText();
                                int nr9 = Integer.parseInt(s9);
                                String s10 = t13.getText();
                                int nr10 = Integer.parseInt(s10);
                                String s11 = t14.getText();
                                int nr11 = Integer.parseInt(s11);
                                String s12 = t15.getText();
                                int nr12 = Integer.parseInt(s12);
                                String s13 = t16.getText();
                                int nr13 = Integer.parseInt(s13);

                                Color ccc = Color.RED;
                                    if(language.getLanguage().equals("romana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier1[47], ccc);
                                    }else if(language.getLanguage().equals("engleza"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier2[47], ccc);
                                    }else if(language.getLanguage().equals("germana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier3[47], ccc);
                                    }
                                if (ccc == null)
                                    ccc = (Color.RED);
                                SimetrieCilindru sim4 = new SimetrieCilindru("SimetrieCilindru", ccc, nr1, nr2, nr3, nr4, nr5, nr9, nr10, nr11, nr12, nr13, nr6, nr7, nr8);
                                float[][] rotationZ = {
                                        {(float) Math.cos(nr8), (float) -Math.sin(nr8), 0},
                                        {(float) Math.sin(nr8), (float) Math.cos(nr8), 0},
                                        {0, 0, 1}
                                };

                                float[][] rotationX = {
                                        {1, 0, 0},
                                        {0, (float) Math.cos(nr6), (float) -Math.sin(nr6)},
                                        {0, (float) Math.sin(nr6), (float) Math.cos(nr6)}
                                };

                                float[][] rotationY = {
                                        {(float) Math.cos(nr7), 0, (float) Math.sin(nr7)},
                                        {0, 1, 0},
                                        {(float) -Math.sin(nr7), 0, (float) Math.cos(nr7)}
                                };

                                float[][] projection = {
                                        {1, 0, 0},
                                        {0, 1, 0}
                                };

                                float[] point = new float[4];
                                point = new float[]{(float) nr1, (float) nr2, (float) nr3};

                                float[] rot = sim4.matmull(rotationZ, point);
                                float[] rot1 = sim4.matmull(rotationY, rot);
                                float[] rot2 = sim4.matmull(rotationX, rot1);
                                float[] vec = sim4.matmull(projection, rot2);

                                float[] point1 = new float[4];
                                point1 = new float[]{(float) nr9, (float) nr10, (float) nr11};

                                float[] rot0 = sim4.matmull(rotationZ, point1);
                                float[] rot11 = sim4.matmull(rotationY, rot0);
                                float[] rot22 = sim4.matmull(rotationX, rot11);
                                float[] vec1 = sim4.matmull(projection, rot22);

                                SimetrieCilindru sim44 = new SimetrieCilindru("SimetrieCilindru", ccc, (int) vec[0], (int) vec[1], nr4, nr5, (int) vec1[0], (int) vec1[1], nr12, nr13);
                                FabricaCorpuri corp = new FabricaCorpuri("SimetrieCilindru", sim44, ccc);
                                corp.setCuloare(ccc);

                                corp.setSize(650, 410);
                                //lista.add(sim44);

                                pp.add(corp);
                                pp.repaint();
                            }
                        }
                    }
                });
                //////////////

                buton33.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == buton33) {
                            if (t1.getText().equals("") || t2.getText().equals("") || t3.getText().equals("") || t7.getText().equals("") || t9.getText().equals("") || t10.getText().equals("") || t11.getText().equals("") || t12.getText().equals("") || t13.getText().equals("") || t14.getText().equals("") || t15.getText().equals("") || t16.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t3.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s5 = t7.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t9.getText();
                                double nr6 = Double.parseDouble(s6);
                                String s7 = t10.getText();
                                double nr7 = Double.parseDouble(s7);
                                String s8 = t11.getText();
                                double nr8 = Double.parseDouble(s8);
                                String s9 = t12.getText();
                                int nr9 = Integer.parseInt(s9);
                                String s10 = t13.getText();
                                int nr10 = Integer.parseInt(s10);
                                String s11 = t14.getText();
                                int nr11 = Integer.parseInt(s11);
                                String s12 = t15.getText();
                                int nr12 = Integer.parseInt(s12);
                                String s13 = t16.getText();
                                int nr13 = Integer.parseInt(s13);

                                Color ccc = Color.RED;
                                //ccc = JColorChooser.showDialog(null, "Alege culoare pentru sfera", ccc);
                                    if(language.getLanguage().equals("romana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier1[50], ccc);
                                    }else if(language.getLanguage().equals("engleza"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier2[50], ccc);
                                    }else if(language.getLanguage().equals("germana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier3[50], ccc);
                                    }
                                if (ccc == null)
                                    ccc = (Color.RED);
                                SimetrieSfera sim3 = new SimetrieSfera("SimetrieSfera", ccc, nr1, nr2, nr3, nr5, nr9, nr10, nr11, nr12, nr13, nr6, nr7, nr8);
                                float[][] rotationZ = {
                                        {(float) Math.cos(nr8), (float) -Math.sin(nr8), 0},
                                        {(float) Math.sin(nr8), (float) Math.cos(nr8), 0},
                                        {0, 0, 1}
                                };

                                float[][] rotationX = {
                                        {1, 0, 0},
                                        {0, (float) Math.cos(nr6), (float) -Math.sin(nr6)},
                                        {0, (float) Math.sin(nr6), (float) Math.cos(nr6)}
                                };

                                float[][] rotationY = {
                                        {(float) Math.cos(nr7), 0, (float) Math.sin(nr7)},
                                        {0, 1, 0},
                                        {(float) -Math.sin(nr7), 0, (float) Math.cos(nr7)}
                                };

                                float[][] projection = {
                                        {1, 0, 0},
                                        {0, 1, 0}
                                };

                                float[] point = new float[4];
                                point = new float[]{(float) nr1, (float) nr2, (float) nr3};

                                float[] rot = sim3.matmull(rotationZ, point);
                                float[] rot1 = sim3.matmull(rotationY, rot);
                                float[] rot2 = sim3.matmull(rotationX, rot1);
                                float[] vec = sim3.matmull(projection, rot2);

                                float[] point1 = new float[4];
                                point1 = new float[]{(float) nr9, (float) nr10, (float) nr11};

                                float[] rot0 = sim3.matmull(rotationZ, point1);
                                float[] rot11 = sim3.matmull(rotationY, rot0);
                                float[] rot22 = sim3.matmull(rotationX, rot11);
                                float[] vec1 = sim3.matmull(projection, rot22);

                                SimetrieSfera sim33 = new SimetrieSfera("SimetrieSfera", ccc, (int) vec[0], (int) vec[1], nr5, (int) vec1[0], (int) vec1[1], nr12, nr13);
                                FabricaCorpuri corp = new FabricaCorpuri("SimetrieSfera", sim33, ccc);
                                corp.setCuloare(ccc);

                                corp.setSize(650, 410);
                                //lista.add(sim33);

                                pp3.add(corp);
                                pp3.repaint();
                            }
                        }
                    }
                });
                /////////////
                buton22.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == buton22) {
                            if (t6.getText().equals("") || t1.getText().equals("") || t2.getText().equals("") || t3.getText().equals("") || t4.getText().equals("") || t5.getText().equals("") || t7.getText().equals("") || t9.getText().equals("") || t10.getText().equals("") || t11.getText().equals("") || t12.getText().equals("") || t13.getText().equals("") || t14.getText().equals("") || t15.getText().equals("") || t16.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t3.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t4.getText();
                                int nr4 = Integer.parseInt(s4);
                                String s5 = t5.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t6.getText();
                                int nr6 = Integer.parseInt(s6);
                                String s7 = t7.getText();
                                int nr7 = Integer.parseInt(s7);
                                String s9 = t12.getText();
                                int nr9 = Integer.parseInt(s9);
                                String s10 = t13.getText();
                                int nr10 = Integer.parseInt(s10);
                                String s11 = t14.getText();
                                int nr11 = Integer.parseInt(s11);
                                String s12 = t15.getText();
                                int nr12 = Integer.parseInt(s12);
                                String s13 = t16.getText();
                                int nr13 = Integer.parseInt(s13);
                                String s14 = t9.getText();
                                double nr14 = Double.parseDouble(s14);
                                String s15 = t10.getText();
                                double nr15 = Double.parseDouble(s15);
                                String s16 = t11.getText();
                                double nr16 = Double.parseDouble(s16);


                                Color ccc = Color.RED;
                                //ccc = JColorChooser.showDialog(null, "Alege culoare pentru Con", ccc);
                                    if(language.getLanguage().equals("romana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier1[49], ccc);
                                    }else if(language.getLanguage().equals("engleza"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier2[49], ccc);
                                    }else if(language.getLanguage().equals("germana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier3[49], ccc);
                                    }
                                if (ccc == null)
                                    ccc = (Color.RED);
                                SimetrieCon sim1 = new SimetrieCon("SimetrieCon", ccc, nr1, nr2, nr3, nr4, nr5, nr6, nr7, nr9, nr10, nr11, nr12, nr13, nr14, nr15, nr16);
                                float[][] rotationZ = {
                                        {(float) Math.cos(nr16), (float) -Math.sin(nr16), 0},
                                        {(float) Math.sin(nr16), (float) Math.cos(nr16), 0},
                                        {0, 0, 1}
                                };

                                float[][] rotationX = {
                                        {1, 0, 0},
                                        {0, (float) Math.cos(nr14), (float) -Math.sin(nr14)},
                                        {0, (float) Math.sin(nr14), (float) Math.cos(nr14)}
                                };

                                float[][] rotationY = {
                                        {(float) Math.cos(nr15), 0, (float) Math.sin(nr15)},
                                        {0, 1, 0},
                                        {(float) -Math.sin(nr15), 0, (float) Math.cos(nr15)}
                                };

                                float[][] projection = {
                                        {1, 0, 0},
                                        {0, 1, 0}
                                };

                                float[] point = new float[4];
                                point = new float[]{(float) nr1, (float) nr2, (float) nr3};

                                float[] rot = sim1.matmull(rotationZ, point);
                                float[] rot1 = sim1.matmull(rotationY, rot);
                                float[] rot2 = sim1.matmull(rotationX, rot1);
                                float[] vec = sim1.matmull(projection, rot2);

                                float[] point1 = new float[4];
                                point1 = new float[]{(float) nr4, (float) nr5, (float) nr6};

                                float[] rot0 = sim1.matmull(rotationZ, point1);
                                float[] rot11 = sim1.matmull(rotationY, rot0);
                                float[] rot22 = sim1.matmull(rotationX, rot11);
                                float[] vec1 = sim1.matmull(projection, rot22);

                                float[] point2 = new float[4];
                                point2 = new float[]{(float) nr9, (float) nr10, (float) nr11};

                                float[] rot00 = sim1.matmull(rotationZ, point2);
                                float[] rot111 = sim1.matmull(rotationY, rot00);
                                float[] rot222 = sim1.matmull(rotationX, rot111);
                                float[] vec2 = sim1.matmull(projection, rot222);

                                SimetrieCon sim11 = new SimetrieCon("SimetrieCon", ccc, (int) vec[0], (int) vec[1], (int) vec1[0], (int) vec1[1], nr7, (int) vec2[0], (int) vec2[1], nr12, nr13);
                                FabricaCorpuri corp = new FabricaCorpuri("SimetrieCon", sim11, ccc);
                                corp.setCuloare(ccc);

                                corp.setSize(650, 410);
                                //lista.add(sim11);

                                pp2.add(corp);
                                pp2.repaint();
                            }
                        }
                    }
                });
                ////////////
                buton11.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Object sursa = e.getSource();
                        if (sursa == buton11) {
                            if (t6.getText().equals("") || t1.getText().equals("") || t2.getText().equals("") || t3.getText().equals("") || t4.getText().equals("") || t5.getText().equals("") || t7.getText().equals("") || t8.getText().equals("") || t9.getText().equals("") || t10.getText().equals("") || t11.getText().equals("") || t12.getText().equals("") || t13.getText().equals("") || t14.getText().equals("") || t15.getText().equals("") || t16.getText().equals("")) {
                                if (language.getLanguage().equals("romana")) {
                                    JOptionPane.showMessageDialog(null, fisier1[51], fisier1[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("engleza")) {
                                    JOptionPane.showMessageDialog(null, fisier2[51], fisier2[52], JOptionPane.ERROR_MESSAGE);
                                } else if (language.getLanguage().equals("germana")) {
                                    JOptionPane.showMessageDialog(null, fisier3[51], fisier3[52], JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            else {
                                String s1 = t1.getText();
                                int nr1 = Integer.parseInt(s1);
                                String s2 = t2.getText();
                                int nr2 = Integer.parseInt(s2);
                                String s3 = t3.getText();
                                int nr3 = Integer.parseInt(s3);
                                String s4 = t4.getText();
                                int nr4 = Integer.parseInt(s4);
                                String s5 = t5.getText();
                                int nr5 = Integer.parseInt(s5);
                                String s6 = t6.getText();
                                int nr6 = Integer.parseInt(s6);
                                String s7 = t7.getText();
                                int nr7 = Integer.parseInt(s7);
                                String s8 = t8.getText();
                                int nr8 = Integer.parseInt(s8);
                                String s9 = t12.getText();
                                int nr9 = Integer.parseInt(s9);
                                String s10 = t13.getText();
                                int nr10 = Integer.parseInt(s10);
                                String s11 = t14.getText();
                                int nr11 = Integer.parseInt(s11);
                                String s12 = t15.getText();
                                int nr12 = Integer.parseInt(s12);
                                String s13 = t16.getText();
                                int nr13 = Integer.parseInt(s13);
                                String s14 = t9.getText();
                                double nr14 = Double.parseDouble(s14);
                                String s15 = t10.getText();
                                double nr15 = Double.parseDouble(s15);
                                String s16 = t11.getText();
                                double nr16 = Double.parseDouble(s16);


                                Color ccc = Color.RED;
                                //ccc = JColorChooser.showDialog(null, "Alege culoare pentru Trunchiul de con", ccc);
                                    if(language.getLanguage().equals("romana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier1[48], ccc);
                                    }else if(language.getLanguage().equals("engleza"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier2[48], ccc);
                                    }else if(language.getLanguage().equals("germana"))
                                    {
                                        ccc = JColorChooser.showDialog(null, fisier3[48], ccc);
                                    }
                                if (ccc == null)
                                    ccc = (Color.RED);
                                SimetrieTrunchiCon sim2 = new SimetrieTrunchiCon("SimetrieTrunchiCon", ccc, nr1, nr2, nr3, nr4, nr5, nr6, nr7, nr8, nr9, nr10, nr11, nr12, nr13, nr14, nr15, nr16);
                                float[][] rotationZ = {
                                        {(float) Math.cos(nr16), (float) -Math.sin(nr16), 0},
                                        {(float) Math.sin(nr16), (float) Math.cos(nr16), 0},
                                        {0, 0, 1}
                                };

                                float[][] rotationX = {
                                        {1, 0, 0},
                                        {0, (float) Math.cos(nr14), (float) -Math.sin(nr14)},
                                        {0, (float) Math.sin(nr14), (float) Math.cos(nr14)}
                                };

                                float[][] rotationY = {
                                        {(float) Math.cos(nr15), 0, (float) Math.sin(nr15)},
                                        {0, 1, 0},
                                        {(float) -Math.sin(nr15), 0, (float) Math.cos(nr15)}
                                };

                                float[][] projection = {
                                        {1, 0, 0},
                                        {0, 1, 0}
                                };

                                float[] point = new float[4];
                                point = new float[]{(float) nr1, (float) nr2, (float) nr3};

                                float[] rot = sim2.matmull(rotationZ, point);
                                float[] rot1 = sim2.matmull(rotationY, rot);
                                float[] rot2 = sim2.matmull(rotationX, rot1);
                                float[] vec = sim2.matmull(projection, rot2);

                                float[] point1 = new float[4];
                                point1 = new float[]{(float) nr4, (float) nr5, (float) nr6};

                                float[] rot0 = sim2.matmull(rotationZ, point1);
                                float[] rot11 = sim2.matmull(rotationY, rot0);
                                float[] rot22 = sim2.matmull(rotationX, rot11);
                                float[] vec1 = sim2.matmull(projection, rot22);

                                float[] point2 = new float[4];
                                point2 = new float[]{(float) nr9, (float) nr10, (float) nr11};

                                float[] rot00 = sim2.matmull(rotationZ, point2);
                                float[] rot111 = sim2.matmull(rotationY, rot00);
                                float[] rot222 = sim2.matmull(rotationX, rot111);
                                float[] vec2 = sim2.matmull(projection, rot222);

                                SimetrieTrunchiCon sim22 = new SimetrieTrunchiCon("SimetrieTrunchiCon", ccc, (int) vec[0], (int) vec[1], (int) vec1[0], (int) vec1[1], nr7, nr8, (int) vec2[0], (int) vec2[1], nr12, nr13);
                                FabricaCorpuri corp = new FabricaCorpuri("SimetrieTrunchiCon", sim22, ccc);
                                corp.setCuloare(ccc);

                                corp.setSize(650, 410);
                                //lista.add(sim22);

                                pp1.add(corp);
                                pp1.repaint();
                            }
                        }
                    }
                });
                ////////////
                usa.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        language.setLanguage(limba2);
                        language.attach(ob2);
                        try {
                            int i=0;
                            File myObj = new File("C:\\Users\\User\\IdeaProjects\\Clt\\src\\Client\\engleza.txt");
                            Scanner myReader = new Scanner(myObj);
                            while (myReader.hasNextLine()) {
                                String data = myReader.nextLine();
                                fisier2[i]=data;
                                i++;
                            }
                            myReader.close();
                        } catch (FileNotFoundException ex) {
                            System.out.println("An error occurred.");
                            ex.printStackTrace();
                        }
                        frame.setTitle(fisier2[0]);
                        buton.setText(fisier2[1]);
                        buton1.setText(fisier2[2]);
                        buton2.setText(fisier2[3]);
                        buton3.setText(fisier2[4]);
                        buton4.setText(fisier2[5]);
                        buton5.setText(fisier2[6]);
                        buton6.setText(fisier2[7]);
                        buton7.setText(fisier2[8]);
                        buton0.setText(fisier2[9]);
                        buton11.setText(fisier2[10]);
                        buton22.setText(fisier2[11]);
                        buton33.setText(fisier2[12]);
                        ll7.setText(fisier2[13]);
                        ll8.setText(fisier2[14]);
                        ll15.setText(fisier2[15]);
                        ll16.setText(fisier2[16]);
                        ll17.setText(fisier2[17]);
                        ll18.setText(fisier2[18]);
                        ll19.setText(fisier2[19]);
                        ll21.setText(fisier2[20]);
                        ll20.setText(fisier2[21]);
                        ll25.setText(fisier2[22]);
                        l5.setText(fisier2[23]);
                        label1.setText(fisier2[24]);
                        label2.setText(fisier2[25]);
                        label3.setText(fisier2[26]);
                        label4.setText(fisier2[27]);
                        label5.setText(fisier2[28]);
                        label6.setText(fisier2[29]);
                        label7.setText(fisier2[30]);
                        label8.setText(fisier2[31]);
                        label9.setText(fisier2[32]);
                        label10.setText(fisier2[33]);
                        label11.setText(fisier2[34]);
                        label12.setText(fisier2[35]);
                        label13.setText(fisier2[36]);
                        label14.setText(fisier2[37]);
                        label15.setText(fisier2[38]);
                        label16.setText(fisier2[39]);
                        label17.setText(fisier2[40]);
                        label18.setText(fisier2[41]);
                        label19.setText(fisier2[42]);
                        file.setText(fisier2[43]);
                        salvare.setText(fisier2[44]);
                        deschide.setText(fisier2[45]);
                        label20.setText(fisier2[46]);
                    }
                });
                ////////////
                romania.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        language.setLanguage(limba1);
                        language.attach(ob1);
                        try {
                            int i=0;
                            File myObj = new File("C:\\Users\\User\\IdeaProjects\\Clt\\src\\Client\\romana.txt");
                            Scanner myReader = new Scanner(myObj);
                            while (myReader.hasNextLine()) {
                                String data = myReader.nextLine();
                                fisier1[i]=data;
                                i++;
                            }
                            myReader.close();
                        } catch (FileNotFoundException ex) {
                            System.out.println("An error occurred.");
                            ex.printStackTrace();
                        }
                        frame.setTitle(fisier1[0]);
                        buton.setText(fisier1[1]);
                        buton1.setText(fisier1[2]);
                        buton2.setText(fisier1[3]);
                        buton3.setText(fisier1[4]);
                        buton4.setText(fisier1[5]);
                        buton5.setText(fisier1[6]);
                        buton6.setText(fisier1[7]);
                        buton7.setText(fisier1[8]);
                        buton0.setText(fisier1[9]);
                        buton11.setText(fisier1[10]);
                        buton22.setText(fisier1[11]);
                        buton33.setText(fisier1[12]);
                        ll7.setText(fisier1[13]);
                        ll8.setText(fisier1[14]);
                        ll15.setText(fisier1[15]);
                        ll16.setText(fisier1[16]);
                        ll17.setText(fisier1[17]);
                        ll18.setText(fisier1[18]);
                        ll19.setText(fisier1[19]);
                        ll21.setText(fisier1[20]);
                        ll20.setText(fisier1[21]);
                        ll25.setText(fisier1[22]);
                        l5.setText(fisier1[23]);
                        label1.setText(fisier1[24]);
                        label2.setText(fisier1[25]);
                        label3.setText(fisier1[26]);
                        label4.setText(fisier1[27]);
                        label5.setText(fisier1[28]);
                        label6.setText(fisier1[29]);
                        label7.setText(fisier1[30]);
                        label8.setText(fisier1[31]);
                        label9.setText(fisier1[32]);
                        label10.setText(fisier1[33]);
                        label11.setText(fisier1[34]);
                        label12.setText(fisier1[35]);
                        label13.setText(fisier1[36]);
                        label14.setText(fisier1[37]);
                        label15.setText(fisier1[38]);
                        label16.setText(fisier1[39]);
                        label17.setText(fisier1[40]);
                        label18.setText(fisier1[41]);
                        label19.setText(fisier1[42]);
                        file.setText(fisier1[43]);
                        salvare.setText(fisier1[44]);
                        deschide.setText(fisier1[45]);
                        label20.setText(fisier1[46]);
                    }
                });
                ////////////
                germania.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        language.setLanguage(limba3);
                        language.attach(ob3);
                        try {
                            int i=0;
                            File myObj = new File("C:\\Users\\User\\IdeaProjects\\Clt\\src\\Client\\germana.txt");
                            Scanner myReader = new Scanner(myObj);
                            while (myReader.hasNextLine()) {
                                String data = myReader.nextLine();
                                fisier3[i]=data;
                                i++;
                            }
                            myReader.close();
                        } catch (FileNotFoundException ex) {
                            System.out.println("An error occurred.");
                            ex.printStackTrace();
                        }
                        frame.setTitle(fisier3[0]);
                        buton.setText(fisier3[1]);
                        buton1.setText(fisier3[2]);
                        buton2.setText(fisier3[3]);
                        buton3.setText(fisier3[4]);
                        buton4.setText(fisier3[5]);
                        buton5.setText(fisier3[6]);
                        buton6.setText(fisier3[7]);
                        buton7.setText(fisier3[8]);
                        buton0.setText(fisier3[9]);
                        buton11.setText(fisier3[10]);
                        buton22.setText(fisier3[11]);
                        buton33.setText(fisier3[12]);
                        ll7.setText(fisier3[13]);
                        ll8.setText(fisier3[14]);
                        ll15.setText(fisier3[15]);
                        ll16.setText(fisier3[16]);
                        ll17.setText(fisier3[17]);
                        ll18.setText(fisier3[18]);
                        ll19.setText(fisier3[19]);
                        ll21.setText(fisier3[20]);
                        ll20.setText(fisier3[21]);
                        ll25.setText(fisier3[22]);
                        l5.setText(fisier3[23]);
                        label1.setText(fisier3[24]);
                        label2.setText(fisier3[25]);
                        label3.setText(fisier3[26]);
                        label4.setText(fisier3[27]);
                        label5.setText(fisier3[28]);
                        label6.setText(fisier3[29]);
                        label7.setText(fisier3[30]);
                        label8.setText(fisier3[31]);
                        label9.setText(fisier3[32]);
                        label10.setText(fisier3[33]);
                        label11.setText(fisier3[34]);
                        label12.setText(fisier3[35]);
                        label13.setText(fisier3[36]);
                        label14.setText(fisier3[37]);
                        label15.setText(fisier3[38]);
                        label16.setText(fisier3[39]);
                        label17.setText(fisier3[40]);
                        label18.setText(fisier3[41]);
                        label19.setText(fisier3[42]);
                        file.setText(fisier3[43]);
                        salvare.setText(fisier3[44]);
                        deschide.setText(fisier3[45]);
                        label20.setText(fisier3[46]);
                    }
                });
                ///////////

                InputStream input = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));

                String time = reader.readLine();


            }while(socket.getKeepAlive());

            socket.close();

        } catch (UnknownHostException ex) {

            System.out.println("Server not found: " + ex.getMessage());

        } catch (IOException ex) {

            System.out.println("I/O error: " + ex.getMessage());
        }
    }
}