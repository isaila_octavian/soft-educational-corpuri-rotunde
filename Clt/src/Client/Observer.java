package Client;

public class Observer implements IObserver{

    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Observer(String language,ILanguage l)
    {
        this.language=language;
        l.attach(this);
    }

    public void update(String language)
    {
        System.out.println("Limba "+language+" este pregatita");
    }
}

