

import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import  javax.swing.*;

public class SimetrieSfera extends Corpuri {
    private int coordonata1;
    private int coordonata2;
    private int coordonata3;
    private int raza;
    private int coodonata4;
    private int coordonata5;
    private int coordonata6;
    private int lungime;
    private int latime;
    private double alfa;
    private double beta;
    private double gama;


    public SimetrieSfera(String nume, Color culoare, int c1, int c2, int raza, int c5, int c6, int lungime, int latime) {
        super(nume,culoare);
        this.coordonata1 = c1;
        this.coordonata2 = c2;
        this.raza = raza;
        this.coordonata5 = c5;
        this.coordonata6 = c6;
        this.lungime = lungime;
        this.latime = latime;
    }

    public SimetrieSfera(String nume, Color culoare, int c1, int c2,int c3, int raza,int c4, int c5, int c6, int lungime, int latime,double alfa,double beta,double gama) {
        super(nume,culoare);
        this.coordonata1 = c1;
        this.coordonata2 = c2;
        this.coordonata3=c3;
        this.raza = raza;
        this.coodonata4=c4;
        this.coordonata5 = c5;
        this.coordonata6 = c6;
        this.lungime = lungime;
        this.latime = latime;
        this.alfa=alfa;
        this.beta=beta;
        this.gama=gama;
    }

    public int getCoordonata1() {
        return coordonata1;
    }

    public void setCoordonata1(int coordonata1) {
        this.coordonata1 = coordonata1;
    }

    public int getCoordonata2() {
        return coordonata2;
    }

    public void setCoordonata2(int coordonata2) {
        this.coordonata2 = coordonata2;
    }

    public int getCoordonata3() {
        return coordonata3;
    }

    public void setCoordonata3(int coordonata3) {
        this.coordonata3 = coordonata3;
    }

    public int getRaza() {
        return raza;
    }

    public void setRaza(int raza) {
        this.raza = raza;
    }

    public int getCoodonata4() {
        return coodonata4;
    }

    public void setCoodonata4(int coodonata4) {
        this.coodonata4 = coodonata4;
    }

    public int getCoordonata5() {
        return coordonata5;
    }

    public void setCoordonata5(int coordonata5) {
        this.coordonata5 = coordonata5;
    }

    public int getCoordonata6() {
        return coordonata6;
    }

    public void setCoordonata6(int coordonata6) {
        this.coordonata6 = coordonata6;
    }

    public int getLungime() {
        return lungime;
    }

    public void setLungime(int lungime) {
        this.lungime = lungime;
    }

    public int getLatime() {
        return latime;
    }

    public void setLatime(int latime) {
        this.latime = latime;
    }

    public double getAlfa() {
        return alfa;
    }

    public void setAlfa(double alfa) {
        this.alfa = alfa;
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public double getGama() {
        return gama;
    }

    public void setGama(double gama) {
        this.gama = gama;
    }

    public float[][] matmul(float[][] a, float[][] b)
    {
        int colsA=a[0].length;
        int rowsA=a.length;
        int colsB=b[0].length;
        int rowsB=b.length;

        if(colsA!=rowsB) {
            System.out.println("Coloanele lui a nu se potrivesc cu randurile lui b");
            return null;
        }
        float[][] result=new float[rowsA][colsB];
        for (int i=0; i<rowsA; i++){
            for(int j=0;j<colsB;j++)
            {
                float sum=0;
                for(int k=0;k<colsA;k++)
                {
                    sum+=a[i][k]*b[k][j];
                }
                result[i][j]=sum;
            }
        }
        return result;
    }

    public float[][] vecToMatrix(float[] v)
    {
        float[][] m=new float[3][1];
        m[0][0]=v[0];
        m[1][0]=v[1];
        m[2][0]=v[2];
        return m;
    }

    public float[] matrixToVec(float[][] m)
    {
        float[] v=new float[3];
        v[0]=m[0][0];
        v[1]=m[1][0];
        if(m.length>2)
            v[2]=m[2][0];
        return v;
    }

    public float[][] matmul(float[][] a,float[] b)
    {
        float[][] m=this.vecToMatrix(b);
        return this.matmul(a,m);
    }

    public float[] matmull(float[][] a,float[] b)
    {
        float[][] m=this.vecToMatrix(b);
        float[][] rez=this.matmul(a,m);
        float[] rezz=this.matrixToVec(rez);
        return rezz;
    }

}



