

import java.awt.*;

public class SimetrieTrunchiCon extends Corpuri{
    private int coordonata1;
    private int coordonata2;
    private int coordonata3;
    private int coordonata4;
    private int raza1;
    private int raza2;
    private int coordonata5;
    private int coordonata6;
    private int coordonata7;
    private int coordonata8;
    private int coordonata9;
    private int lungime;
    private int latime;
    private double alfa;
    private double beta;
    private double gama;

    public SimetrieTrunchiCon(String nume, Color culoare, int c1, int c2, int c3, int c4, int raza1,int raza2, int coordonata5, int coordonata6, int lungime, int latime) {
        super(nume,culoare);
        this.coordonata1 = c1;
        this.coordonata2 = c2;
        this.coordonata3 = c3;
        this.coordonata4 = c4;
        this.raza1 = raza1;
        this.raza2=raza2;
        this.coordonata5=coordonata5;
        this.coordonata6=coordonata6;
        this.lungime=lungime;
        this.latime=latime;
    }

    public SimetrieTrunchiCon(String nume, Color culoare, int c1, int c2, int c3, int c4,int c5,int c6, int raza1,int raza2, int c7, int c8,int c9, int lungime, int latime,double alfa,double beta, double gama) {
        super(nume,culoare);
        this.coordonata1 = c1;
        this.coordonata2 = c2;
        this.coordonata3 = c3;
        this.coordonata4 = c4;
        this.coordonata5=c5;
        this.coordonata6=c6;
        this.raza1 = raza1;
        this.raza2=raza2;
        this.coordonata5=c7;
        this.coordonata6=c8;
        this.coordonata9=c9;
        this.lungime=lungime;
        this.latime=latime;
        this.alfa=alfa;
        this.beta=beta;
        this.gama=gama;
    }

    public int getCoordonata1() {
        return coordonata1;
    }

    public void setCoordonata1(int coordonata1) {
        this.coordonata1 = coordonata1;
    }

    public int getCoordonata2() {
        return coordonata2;
    }

    public void setCoordonata2(int coordonata2) {
        this.coordonata2 = coordonata2;
    }

    public int getCoordonata3() {
        return coordonata3;
    }

    public void setCoordonata3(int coordonata3) {
        this.coordonata3 = coordonata3;
    }

    public int getCoordonata4() {
        return coordonata4;
    }

    public void setCoordonata4(int coordonata4) {
        this.coordonata4 = coordonata4;
    }

    public int getRaza1() {
        return raza1;
    }

    public void setRaza1(int raza1) {
        this.raza1 = raza1;
    }

    public int getRaza2() {
        return raza2;
    }

    public void setRaza2(int raza2) {
        this.raza2 = raza2;
    }

    public int getCoordonata5() {
        return coordonata5;
    }

    public void setCoordonata5(int coordonata5) {
        this.coordonata5 = coordonata5;
    }

    public int getCoordonata6() {
        return coordonata6;
    }

    public void setCoordonata6(int coordonata6) {
        this.coordonata6 = coordonata6;
    }

    public int getCoordonata7() {
        return coordonata7;
    }

    public void setCoordonata7(int coordonata7) {
        this.coordonata7 = coordonata7;
    }

    public int getCoordonata8() {
        return coordonata8;
    }

    public void setCoordonata8(int coordonata8) {
        this.coordonata8 = coordonata8;
    }

    public int getCoordonata9() {
        return coordonata9;
    }

    public void setCoordonata9(int coordonata9) {
        this.coordonata9 = coordonata9;
    }

    public int getLungime() {
        return lungime;
    }

    public void setLungime(int lungime) {
        this.lungime = lungime;
    }

    public int getLatime() {
        return latime;
    }

    public void setLatime(int latime) {
        this.latime = latime;
    }

    public double getAlfa() {
        return alfa;
    }

    public void setAlfa(double alfa) {
        this.alfa = alfa;
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public double getGama() {
        return gama;
    }

    public void setGama(double gama) {
        this.gama = gama;
    }
    public float[][] matmul(float[][] a, float[][] b)
    {
        int colsA=a[0].length;
        int rowsA=a.length;
        int colsB=b[0].length;
        int rowsB=b.length;

        if(colsA!=rowsB) {
            System.out.println("Coloanele lui a nu se potrivesc cu randurile lui b");
            return null;
        }
        float[][] result=new float[rowsA][colsB];
        for (int i=0; i<rowsA; i++){
            for(int j=0;j<colsB;j++)
            {
                float sum=0;
                for(int k=0;k<colsA;k++)
                {
                    sum+=a[i][k]*b[k][j];
                }
                result[i][j]=sum;
            }
        }
        return result;
    }

    public float[][] vecToMatrix(float[] v)
    {
        float[][] m=new float[3][1];
        m[0][0]=v[0];
        m[1][0]=v[1];
        m[2][0]=v[2];
        return m;
    }

    public float[] matrixToVec(float[][] m)
    {
        float[] v=new float[3];
        v[0]=m[0][0];
        v[1]=m[1][0];
        if(m.length>2)
            v[2]=m[2][0];
        return v;
    }

    public float[][] matmul(float[][] a,float[] b)
    {
        float[][] m=this.vecToMatrix(b);
        return this.matmul(a,m);
    }

    public float[] matmull(float[][] a,float[] b)
    {
        float[][] m=this.vecToMatrix(b);
        float[][] rez=this.matmul(a,m);
        float[] rezz=this.matrixToVec(rez);
        return rezz;
    }

}

