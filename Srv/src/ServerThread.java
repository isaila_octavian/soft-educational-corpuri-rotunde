

import javax.swing.*;
import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * This thread is responsible to handle client connection.
 *
 * @author www.codejava.net
 */
public class ServerThread extends Thread {
    private Socket socket;
    private String text1;

    public ServerThread(Socket socket) {
        super("INSERT UPDATE DELETE");
        this.socket = socket;
    }

    public void run() {
        try {
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);

            do{
                text1 = reader.readLine();
                System.out.println(text1);
                if (text1.equals("Cilindru") && text1!=null)
                {
                    String c="Cilindru";
                    writer.println(c);
                }
                else if (text1.equals("Con") && text1!=null)
                {
                    String c="Con";
                    writer.println(c);
                }
                else if (text1.equals("TrunchiCon") && text1!=null)
                {
                    String c="TrunchiCon";
                    writer.println(c);
                }
                else if (text1.equals("Sfera") && text1!=null)
                {
                    String c="Sfera";
                    writer.println(c);
                }

            }while(socket.getKeepAlive());


            socket.close();
        }catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

}