

import java.awt.*;
import java.awt.event.*;
import  javax.swing.*;

public class Sfera extends Corpuri{
    private int coordonata1;
    private int coordonata2;
    private int coordonata3;
    private int raza;
    private Color c;
    private double alfa;
    private double beta;
    private double gama;

    public Sfera(String nume, Color culoare, int coordonata1, int coordonata2, int raza)
    {
        super(nume,culoare);
        this.coordonata1=coordonata1;
        this.coordonata2=coordonata2;
        this.raza=raza;
    }

    public Sfera(String nume, Color culoare, int coordonata1, int coordonata2,int coordonata3, int raza,double alfa,double beta,double gama)
    {
        super(nume,culoare);
        this.coordonata1=coordonata1;
        this.coordonata2=coordonata2;
        this.coordonata3=coordonata3;
        this.raza=raza;
        this.alfa=alfa;
        this.beta=beta;
        this.gama=gama;
    }

    public int getCoordonata1() {
        return coordonata1;
    }

    public void setCoordonata1(int coordonata1) {
        this.coordonata1 = coordonata1;
    }

    public int getCoordonata2() {
        return coordonata2;
    }

    public void setCoordonata2(int coordonata2) {
        this.coordonata2 = coordonata2;
    }

    public int getCoordonata3() {
        return coordonata3;
    }

    public void setCoordonata3(int coordonata3) {
        this.coordonata3 = coordonata3;
    }

    public int getRaza() {
        return raza;
    }

    public void setRaza(int raza) {
        this.raza = raza;
    }

    public Color getC() {
        return c;
    }

    public void setC(Color c) {
        this.c = c;
    }

    public double getAlfa() {
        return alfa;
    }

    public void setAlfa(double alfa) {
        this.alfa = alfa;
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public double getGama() {
        return gama;
    }

    public void setGama(double gama) {
        this.gama = gama;
    }

    public double getArie(int co)
    {
        double suma;
        suma=4*Math.PI*co*co;
        return suma;
    }

    public double getVolum(int co)
    {
        double suma;
        suma=4*Math.PI*co*co*co;
        return suma;
    }
    public float[][] matmul(float[][] a, float[][] b)
    {
        int colsA=a[0].length;
        int rowsA=a.length;
        int colsB=b[0].length;
        int rowsB=b.length;

        if(colsA!=rowsB) {
            System.out.println("Coloanele lui a nu se potrivesc cu randurile lui b");
            return null;
        }
        float[][] result=new float[rowsA][colsB];
        for (int i=0; i<rowsA; i++){
            for(int j=0;j<colsB;j++)
            {
                float sum=0;
                for(int k=0;k<colsA;k++)
                {
                    sum+=a[i][k]*b[k][j];
                }
                result[i][j]=sum;
            }
        }
        return result;
    }

    public float[][] vecToMatrix(float[] v)
    {
        float[][] m=new float[3][1];
        m[0][0]=v[0];
        m[1][0]=v[1];
        m[2][0]=v[2];
        return m;
    }

    public float[] matrixToVec(float[][] m)
    {
        float[] v=new float[3];
        v[0]=m[0][0];
        v[1]=m[1][0];
        if(m.length>2)
            v[2]=m[2][0];
        return v;
    }

    public float[][] matmul(float[][] a,float[] b)
    {
        float[][] m=this.vecToMatrix(b);
        return this.matmul(a,m);
    }

    public float[] matmull(float[][] a,float[] b)
    {
        float[][] m=this.vecToMatrix(b);
        float[][] rez=this.matmul(a,m);
        float[] rezz=this.matrixToVec(rez);
        return rezz;
    }

}

