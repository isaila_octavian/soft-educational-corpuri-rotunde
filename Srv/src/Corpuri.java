

import javax.swing.*;
import java.awt.*;

public class Corpuri extends JPanel {
    private String nume;
    private Color culoare;

    public Corpuri()
    {}

    public Corpuri(String nume, Color culoare)
    {
        this.nume=nume;
        this.culoare=culoare;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Color getCuloare() {
        return culoare;
    }

    public void setCuloare(Color culoare) {
        this.culoare = culoare;
    }
}
